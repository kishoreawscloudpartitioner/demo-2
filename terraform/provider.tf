terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.58"
    }
  }
}

terraform {
  backend "s3" {
    bucket = "terraformbucket20"
    key    = "dev/terraform.tfstate"
    region = "us-east-1"
  }
}

# Configure and downloading plugins for aws
provider "aws" {
  region     = "us-east-1"
}
