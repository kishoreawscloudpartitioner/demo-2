endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/runtime/getdata
method : POST
request body : {
		start_date: "07/15/2022",
		end_date: "07/17/2022",
		machine_id: "DSF0376",
		operator_no: "1235",
		workorder_no: "6300582",
}
response: (status code = 200) [
	{
		"id": "101",
		"work_order_no": "6300582",
		"site": "Kirkland",
		"machine_id": "DSF0370",
		"operator_no": "1235",
		"part_no": "CP1438303",
		"operation_no": "8000",
		"produce_part": 5,
		"good_part": 3,
		"start_time": "2022-07-14T19:35:00.000Z",
		"end_time": "2022-07-14T20:40:00.000Z"
	},
	{
		"id": "102",
		"work_order_no": "6300582",
		"site": "Kirkland",
		"machine_id": "DSF0370",
		"operator_no": "1235",
		"part_no": "CP1438303",
		"operation_no": "9060",
		"produce_part": 4,
		"good_part": 3,
		"start_time": "2022-07-14T21:50:00.000Z",
		"end_time": "2022-07-14T23:20:00.000Z"
	},
	.....
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/runtime/singledata
method : POST
request body : {
		id: 101
}
response: (status code = 200) {
	"good_part": 3,
	"start_date": "14/6/2022",
	"start_time": "19:35",
	"end_date": "14/6/2022",
	"end_time": "20:40"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/runtime/update
method : POST
request body : {
		id: 101,
		good_part: 4,
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Update complete"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/runtime/delete
method : POST
request body : {
		id: 101
}
response: (status code = 201) {
	"message": "Delete complete"
}



endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/setuptime/getdata
method : POST
request body : {
		start_date: "08/24/2022",
		end_date: "08/24/2022",
		machine_id: "DSF0375",
		operator_no: "45678",
		workorder_no: "6361078",
}
response: (status code = 200) [
	{
		"id": "252",
		"work_order_no": "6361078",
		"site": "Krikland",
		"machine_id": "DSF0375",
		"operator_no": "45678",
		"part_no": "CP1433210",
		"operation_no": "9010",
		"activity_type_id": "100001",
		"start_time": "2022-08-24T05:14:43.498Z",
		"end_time": "2022-08-24T05:14:47.613Z"
	},
	{
		"id": "261",
		"work_order_no": "6361078",
		"site": "Krikland",
		"machine_id": "DSF0375",
		"operator_no": "45678",
		"part_no": "CP1433210",
		"operation_no": "9010",
		"activity_type_id": "100001",
		"start_time": "2022-08-24T05:38:09.949Z",
		"end_time": "2022-08-24T05:38:17.937Z"
	}
]

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/setuptime/singledata
method : POST
request body : {
		id: 106
}
response: (status code = 200) {
	"start_date": "8/10/2022",
	"start_time": "17:25",
	"end_date": "8/10/2022",
	"end_time": "17:26"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/setuptime/update
method : POST
request body : {
		id: 106,
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Update complete"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/setuptime/delete
method : POST
request body : {
		id: 106
}
response: (status code = 201) {
	"message": "Delete complete"
}



endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/load/getdata
method : POST
request body : {
		start_date: "08/24/2022",
		end_date: "08/24/2022",
		machine_id: "DSF0375",
		operator_no: "",
		workorder_no: "",
}
response: (status code = 200) [
	{
		"id": "230",
		"work_order_no": "6300582",
		"site": "Krikland",
		"machine_id": "DSF0375",
		"operator_no": "9876",
		"part_no": "CP1438303",
		"operation_no": "8000",
		"activity_type_id": "100002",
		"start_time": "2022-08-24T10:21:11.315Z",
		"end_time": "2022-08-24T10:21:19.998Z"
	},
	....
]

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/load/singledata
method : POST
request body : {
		id: 230
}
response: (status code = 200) {
	"start_date": "8/24/2022",
	"start_time": "10:21",
	"end_date": "8/24/2022",
	"end_time": "10:21"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/load/update
method : POST
request body : {
		id: 230,
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Update complete"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/load/delete
method : POST
request body : {
		id: 230
}
response: (status code = 201) {
	"message": "Delete complete"
}



endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/unload/getdata
method : POST
request body : {
		start_date: "08/24/2022",
		end_date: "08/24/2022",
		machine_id: "",
		operator_no: "45678",
		workorder_no: "",
}
response: (status code = 200) [
	{
		"id": "253",
		"work_order_no": "6361078",
		"site": "Krikland",
		"machine_id": "DSF0375",
		"operator_no": "45678",
		"part_no": "CP1433210",
		"operation_no": "9010",
		"activity_type_id": "100005",
		"start_time": "2022-08-24T05:14:50.708Z",
		"end_time": "2022-08-24T05:15:04.438Z"
	},
	...
]

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/unload/singledata
method : POST
request body : {
		id: 231
}
response: (status code = 200) {
	"start_date": "8/24/2022",
	"start_time": "10:21",
	"end_date": "8/24/2022",
	"end_time": "10:21"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/unload/update
method : POST
request body : {
		id: 231,
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Update complete"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/unload/delete
method : POST
request body : {
		id: 231
}
response: (status code = 201) {
	"message": "Delete complete"
}


endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/pause/getdata
method : POST
request body : {
		activity_type_id: 100003
		start_date: "08/24/2022",
		end_date: "08/24/2022",
		machine_id: "",
		operator_no: "45678",
		workorder_no: "",
}
response: (status code = 200) [
	{
		"id": "117",
		"work_order_no": "6300582",
		"site": "Krikland",
		"machine_id": "DSF0375",
		"operator_no": "9876",
		"part_no": "CP1438303",
		"operation_no": "8000",
		"activity_type_id": "100003",
		"start_time": "2022-08-11T13:48:23.239Z",
		"end_time": "2022-08-11T14:20:06.239Z"
	},
	...
]

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/pause/singledata
method : POST
request body : {
		id: 231,
		activity_type_id: 100003
}
response: (status code = 200) {
	"start_date": "8/11/2022",
	"start_time": "13:48",
	"end_date": "8/11/2022",
	"end_time": "14:23"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/pause/update
method : POST
request body : {
		id: 231,
		activity_type_id: 100003
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Update complete"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintainpart/pause/delete
method : POST
request body : {
		id: 230,
		activity_type_id: 100003
}
response: (status code = 201) {
	"message": "Delete complete"
}



endpoint : http://localhost:3000/safran-iot/api/admin/pending/getdata
method : POST
request body : {
		start_date: "08/22/2022",
		end_date: "08/24/2022",
		machine_id: "",
		operator_no: "",
		workorder_no: "",
}
response: (status code = 200) {
	"data": [
		{
			"id": "102",
			"work_order_no": "6300582",
			"site": "safran",
			"machine_id": "DSF0375",
			"operator_no": "09876",
			"part_no": "CP1438303",
			"operation_no": "8000",
			"activity_type_id": "1003",
			"reason_id": "10013",
			"start_time": "2022-08-22T14:03:25.965Z",
			"end_time": "2022-08-22T14:07:21.303Z",
			"modified_by": null,
			"status": "Insert"
		},
	]
}

endpoint : http://localhost:3000/safran-iot/api/admin/pending/singledata
method : POST
request body : {
		id: 102,
		activity_type_id: 1003
}
response: (status code = 200) {
	"start_date": "8/22/2022",
	"start_time": "10:0",
	"end_date": "8/22/2022",
	"end_time": "12:0",
	"single_data": {
		"id": "101",
		"work_order_no": "6300582",
		"site": "DSF0376",
		"machine_id": "safran",
		"operator_no": null,
		"part_no": "CP1438303",
		"operation_no": "8000",
		"activity_type_id": "1003",
		"reason_id": "10013",
		"start_time": "2022-08-22T10:00:00.000Z",
		"end_time": "2022-08-22T12:00:00.000Z",
		"modified_by": "0000",
		"status": "Delete"
	}
}

endpoint : http://localhost:3000/safran-iot/api/admin/pending/update
method : POST
request body : {
		id: 102,
		activity_type_id: 1003,
		reason_id: 10013
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Pending Update complete for id 102"
}

endpoint : http://localhost:3000/safran-iot/api/admin/pending/delete
method : POST
request body : {
		id: 102,
}
response: (status code = 201) {
	"message": "Pending Delete complete for id 102"
}



endpoint : http://localhost:3000/safran-iot/api/admin/replacetool/getdata
method : POST
request body : {
		start_date: "07/18/2022",
		end_date: "07/28/2022",
		machine_id: "",
		operator_no: "",
}
response: (status code = 200) {
	"data": [
		{
			"activity_id": "136",
			"site": "safran",
			"machine_id": "1234",
			"operator_no": "1235",
			"activity_type_id": "1002",
			"reason_id": null,
			"start_time": "2022-07-21T11:14:32.873Z",
			"end_time": "2022-07-21T11:14:46.248Z",
			"description": null,
			"modified_by": null,
			"status": "Insert"
		},
	]
}

endpoint : http://localhost:3000/safran-iot/api/admin/replacetool/singledata
method : POST
request body : {
		id: 134,
		activity_type_id: 1002
}
response: (status code = 200) {
	"operator_no": "1235",
	"start_date": "8/22/2022",
	"start_time": "15:30",
	"end_date": "8/22/2022",
	"end_time": "17:30",
	"description": "hello test"
}

endpoint : http://localhost:3000/safran-iot/api/admin/replacetool/update
method : POST
request body : {
		id: 134,
		activity_type_id: 1002,
		description: "test description"
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Replace tool Update complete for id 134"
}

endpoint : http://localhost:3000/safran-iot/api/admin/replacetool/delete
method : POST
request body : {
		id: 134,
}
response: (status code = 201) {
	"message": "Replace tool Delete complete for id 134"
}



endpoint : http://localhost:3000/safran-iot/api/admin/maintenance/getdata
method : POST
request body : {
		start_date: "07/18/2022",
		end_date: "07/28/2022",
		machine_id: "",
		operator_no: "",
}
response: (status code = 200) {
	"data": [
		{
			"activity_id": "144",
			"site": "safran",
			"machine_id": "DSF0375",
			"operator_no": "1235",
			"activity_type_id": "1004",
			"reason_id": "10001",
			"start_time": "2022-07-21T12:37:06.912Z",
			"end_time": "2022-07-21T12:37:12.563Z",
			"description": null,
			"modified_by": null,
			"status": "Insert",
			"reason": "Cleaning"
		}
	]
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintenance/singledata
method : POST
request body : {
		id: 138,
		activity_type_id: 1004
}
response: (status code = 200) {
	"operator_no": "1235",
	"start_date": "8/22/2022",
	"start_time": "15:30",
	"end_date": "8/22/2022",
	"end_time": "19:30",
	"reason_id": "10002",
	"reasons_list": [
		{
			"r_reason_id": "10001",
			"r_reason": "Cleaning"
		},
		{
			"r_reason_id": "10002",
			"r_reason": "Daily calibration"
		}
	]
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintenance/update
method : POST
request body : {
		id: 138,
		activity_type_id: 1004,
		reason_id: 10002
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Maintenance Update complete for id 138"
}

endpoint : http://localhost:3000/safran-iot/api/admin/maintenance/delete
method : POST
request body : {
		id: 138,
}
response: (status code = 201) {
	"message": "Maintenance Delete complete for id 138"
}



endpoint : http://localhost:3000/safran-iot/api/admin/outoforder/getdata
method : POST
request body : {
		start_date: "07/18/2022",
		end_date: "07/28/2022",
		machine_id: "",
		operator_no: "",
}
response: (status code = 200) {
	"data": [
		{
			"activity_id": "160",
			"site": "safran",
			"machine_id": "DSF0375",
			"operator_no": "09876",
			"activity_type_id": "1005",
			"reason_id": "10009",
			"start_time": "2022-07-28T10:33:43.370Z",
			"end_time": "2022-07-28T10:35:13.621Z",
			"description": null,
			"modified_by": null,
			"status": "Insert",
			"reason": "Pending Maintenance Personnel"
		},
	]
}

endpoint : http://localhost:3000/safran-iot/api/admin/outoforder/singledata
method : POST
request body : {
		id: 178,
		activity_type_id: 1005
}
response: (status code = 200) {
	"operator_no": "1111",
	"start_date": "8/22/2022",
	"start_time": "15:30",
	"end_date": "8/22/2022",
	"end_time": "19:30",
	"reason_id": "10011",
	"reasons_list": [
		{
			"r_reason_id": "10010",
			"r_reason": "Mechanical Failure"
		},
		{
			"r_reason_id": "10011",
			"r_reason": "Pneumatic Failure"
		},
		{
			"r_reason_id": "10012",
			"r_reason": "Vacuum Failure"
		}
	]
}

endpoint : http://localhost:3000/safran-iot/api/admin/outoforder/update
method : POST
request body : {
		id: 178,
		activity_type_id: 1005,
		reason_id: 10011
		start_time: "07/20/2022T19:00:00",
		ens_time: "07/21/2022T20:00:00"
}
response: (status code = 201) {
	"message": "Out of Order Update complete for id 178"
}

endpoint : http://localhost:3000/safran-iot/api/admin/outoforder/delete
method : POST
request body : {
		id: 178,
}
response: (status code = 201) {
	"message": "Out of Order Delete complete for id 178"
}