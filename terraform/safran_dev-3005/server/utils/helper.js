const pool = require('../config/db')

async function query_runner (sql_query) {
    try {
        const data = await pool.query(sql_query)
        return data
    } catch (err) {
        return err
    }
}

function get_date(date) {
    const convert_date = new Date(date)
    return `${convert_date.getUTCMonth()+1}/${convert_date.getUTCDate()}/${convert_date.getUTCFullYear()}`
}

function get_time(date) {
    const convert_time = new Date(date)
    return `${convert_time.getUTCHours()}:${convert_time.getUTCMinutes()}`
}

module.exports = {
    get_date,
    get_time,
    query_runner
}