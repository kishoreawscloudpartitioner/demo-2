jQuery(document).ready(function($){
   
    $.validator_formID("outoford_form");
    function outoforder_dropdown(id,url,reason)
    {
        if ($("#"+id).exists()) {
            $("#"+id).select2({
                placeholder: "Select",
            });
            $("#"+id).empty();
            $("#"+id).append('<option value="null" disabled="disabled">Data loading...</option>');
            var dataJSON = JSON.stringify({
                machine_type: "3 Axis Router",
                activity_type_id: "1005",
                reason_for:reason
            });
            $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#"+id).empty();
                },
                success: function (data)
                {
                    $("#"+id).select2('close');
                    $("#"+id).empty();
                    $("#"+id).append('<option  value=""></option>');
                    $.each(data, function(key, entry) {
                        if(entry.reason)
                        {
                            $("#"+id).append('<option  value="' + entry.reason_id +'">' + entry.reason + '</option>');
                        }
                        
                    });
                
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#"+id).append('<option value="null" disabled="disabled">Data can not be load!</option>');
                },
            });
        }
    }
    
    outoforder_dropdown("outoford_op_reason","/safran-iot/api/outoforder/dropdown","Operator");
    outoforder_dropdown("outoford_maint_reason","/safran-iot/api/outoforder/dropdown","Maintenance");
    
    // $('#outoford_subbtnn').on('click', function (e) {
    //     if ($("#outoford_form").valid()) {
    //         $("#outoford_subbtn").prop("disabled", true);
    //         var all_Id;
    //         if ($("#outoford_op_reason").exists())r
    //         {
    //             all_Id=$("#outoford_op_reason").val().split("-");
    //         }
    //         else
    //         {
    //             all_Id=$("#outoford_maint_reason").val().split("-");
    //         }
    //         var dataJSON = JSON.stringify({
    //            site_id :"1",
    //             machine_id: "DSF0375",
    //             activity_type_id:all_Id[1],
    //             reason_id: all_Id[0]
    //         });
    //         window.location = "/menu";
    //         // $.ajax({
    //         //     url: "safran-iot/api/outoforder/start",
    //         //     type: "POST",
    //         //     contentType: "application/json",
    //         //     data: dataJSON,
    //         //     beforeSend: function () {
    //         //         $("#outoford_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
    //         //     },
    //         //     success: function (data)
    //         //     {
    //         //         window.location = "/finish";
    //         //         $("#outoford_subbtn span").html("");
    //         //     },
    //         //     error: function (jqXhr, textStatus, errorThrown) {
    //         //         $("#outoford_subbtn span").html("");
    //         //         $("#outoford_subbtn").prop("disabled", false);
    //         //     },
    //         // });
    //      }
    // });

});