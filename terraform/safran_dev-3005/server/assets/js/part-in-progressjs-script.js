jQuery(document).ready(function($){
    var totalSeconds = 0;
    var timerVar;
    function countTimer() {
        if(arguments.length)
        {
            var hms = time   // your input string
            var a = hms.split(':'); // split it at the colons
            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            totalSeconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
            ++totalSeconds;
            
        }
        else{
           
            ++totalSeconds;
        }
       
        var hour = Math.floor(totalSeconds /3600);
        var minute = Math.floor((totalSeconds - hour*3600)/60);
        var seconds = totalSeconds - (hour*3600 + minute*60);
        if(hour < 10)
            hour = "0"+hour;
        if(minute < 10)
            minute = "0"+minute;
        if(seconds < 10)
            seconds = "0"+seconds;
            if ($("#timer").exists()) {
           
                document.getElementById("timer").innerHTML = hour + ":" + minute + ":" + seconds;
        
        }
    }
    $('#partPrg_finsh').on('click', function (e) {
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id: 100005,
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/finishpart",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#partPrg_finsh span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/part-complete";
                $("#partPrg_finsh span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#partPrg_finsh span").html("");
                $("#partPrg_finsh").prop("disabled", false);
            },
        });
       
    });
    $('#partPrg_RTpause').on('click', function (e) {
        var timeFlag="pause";
        // if($('#partPrg_RTpause').text()=="Pause - Replace Tool")
        // {
        //     timeFlag="pause";
        //     $('#partPrg_RTpause').text("Resume - Replace Tool");
        //     $("#partPrg_finsh").prop("disabled", true);
        //     $("#partPrg_pause").prop("disabled", true);
            
        // }
        // else{
        //     timeFlag="resume";
        //     $('#partPrg_RTpause').text("Pause - Replace Tool");
        //     $("#partPrg_finsh").prop("disabled", false);
        //     $("#partPrg_pause").prop("disabled", false);
           
        // }
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id: 100003,
            pause: timeFlag,
            comment: ""
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/pause",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#partPrg_RTpause span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                $("#partPrg_RTpause span").html("");
                $('.modal-container').load("/part-replace-pause-popup",function(result) {
                    $("#repalce_pause_popup").modal('show');
                    countTimer();
                    timerVar=setInterval(countTimer, 1000);
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#partPrg_RTpause span").html("");
            },
        });
       
    });
    $(document).on("click", "#partPrg_RT_resume", function (e){
        $("#partPrg_RT_resume").prop("disabled", true);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id: 100003,
            pause: "resume",
            comment: $("#replaceNote").val()
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/pause",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#partPrg_RT_resume span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                clearInterval(timerVar);
                totalSeconds = 0;
                $("#repalce_pause_popup").modal('hide');
            
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#partPrg_RT_resume").prop("disabled", false);
            },
        });
    });
    $('#partPrg_pause').on('click', function (e) {
        var timeFlag="pause";
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id: 100004,
            pause: timeFlag,
            comment: ""
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/pause",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#partPrg_pause span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                $("#partPrg_pause span").html("");
                $('.modal-container').load("/part-pause-popup",function(result) {
                    $("#part_pause_popup").modal('show');
                    countTimer();
                    timerVar=setInterval(countTimer, 1000);
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#partPrg_pause span").html("");
            },
        });
       
    });
    $(document).on("click", "#part_resume", function (e){
        $("#part_resume").prop("disabled", true);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id: 100004,
            pause: "resume",
            comment: $("#pauseNote").val()
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/pause",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#part_resume span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                clearInterval(timerVar);
                totalSeconds = 0;
                $("#part_pause_popup").modal('hide');
            
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#part_resume").prop("disabled", false);
            },
        });
    });

    $('#partPrg_pausetttt').on('click', function (e) {
        var timeFlag="pause";
        if($('#partPrg_pause').text()=="Pause")
        {
            timeFlag="pause";
            $('#partPrg_pause').text("Resume");
            $("#partPrg_finsh").prop("disabled", true);
            $("#partPrg_RTpause").prop("disabled", true);
           
        }
        else{
            timeFlag="resume";
            $('#partPrg_pause').text("Pause");
            $("#partPrg_finsh").prop("disabled", false);
            $("#partPrg_RTpause").prop("disabled", false);
           
        }
       
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id: 100004,
            pause: timeFlag 
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/pause",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#pm_startBtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                $("#pm_startBtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#pm_startBtn span").html("");
                $("#pm_startBtn").prop("disabled", false);
            },
        });
       
    });
});