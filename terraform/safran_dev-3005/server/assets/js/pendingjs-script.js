jQuery(document).ready(function($){
  $.validator_formID("pending_form");
    $('#pending_opreason').select2({
      placeholder: "Select",
    });
    var dataJSON = JSON.stringify({
      machine_type: "3 Axis Router",
      activity_type_id: "1003",
    });
    $("#pending_opreason").empty();
    $("#pending_opreason").append('<option value="null" disabled="disabled">Data loading...</option>');
    $.ajax({
      url: "/safran-iot/api/pending/reasons",
      type: "POST",
      contentType: "application/json",
      data: dataJSON,
      beforeSend: function () {
        $("#pending_opreason").empty();
      },
      success: function (data)
      {
        $("#pending_opreason").append('<option  value=""></option>');
        $.each(data, function(key, entry) {
          if(entry.reason_id)
          {
              $("#pending_opreason").append('<option  value="'+entry.reason_id+'">' + entry.reason + '</option>');
          }
          $("#pend_startBtn").prop("disabled", false);
        });
      
      },
      error: function (jqXhr, textStatus, errorThrown) {
        $("#pending_opreason").append('<option value="null" disabled="disabled">Data can not be load!</option>');
      },
  });
    // $(document).on("change", "#pending_opCode", function () {
    //     var opCode=$('#pending_opCode').val();
    //     $('#opNum').val(opCode.slice(opCode.length - 4));
    //     var WrkOpNum=opCode.slice(opCode.length - 11);
    //     $('#wrkorder').val(WrkOpNum.slice(0,-4));
       
    //     //remaming data fill
    //     var dataJSON = JSON.stringify({
    //       workorder_no: $('#wrkorder').val(),
    //       operation_no: $('#opNum').val(),
    //     });
    //     $.ajax({
    //         url: "/safran-iot/api/partmenu",
    //         type: "POST",
    //         contentType: "application/json",
    //         data: dataJSON,
    //         success: function (data)
    //         {
    //           $("#partNum").val(data.part_no);
    //           var dataJSON = JSON.stringify({
    //             machine_type: "3 Axis Router",
    //             activity_type_id: "1003",
    //           });
    //           $("#pending_opreason").empty();
    //           $("#pending_opreason").append('<option value="null" disabled="disabled">Data loading...</option>');
    //           $.ajax({
    //             url: "/safran-iot/api/pending/reasons",
    //             type: "POST",
    //             contentType: "application/json",
    //             data: dataJSON,
    //             beforeSend: function () {
    //               $("#pending_opreason").empty();
    //             },
    //             success: function (data)
    //             {
    //               $("#pending_opreason").append('<option  value=""></option>');
    //               $.each(data, function(key, entry) {
    //                 if(entry.reason_id)
    //                 {
    //                     $("#pending_opreason").append('<option  value="'+entry.reason_id+'">' + entry.reason + '</option>');
    //                 }
    //                 $("#pend_startBtn").prop("disabled", false);
    //               });
                
    //             },
    //             error: function (jqXhr, textStatus, errorThrown) {
    //               $("#pending_opreason").append('<option value="null" disabled="disabled">Data can not be load!</option>');
    //             },
    //         });

            
    //         },
    //         error: function (jqXhr, textStatus, errorThrown) {
    //             $("#pend_startBtn").prop("disabled", true);
    //         },
    //     });
        
    // });
       
    // $('#pend_startBtn').on('click', function (e) {

    //   e.preventDefault();
    //   $("#pend_startBtn").prop("disabled", true);
    //   var dataJSON = JSON.stringify({
    //     workorder_no: $('#wrkorder').val(),
    //     operation_no: $('#opNum').val(),
    //    site_id :"1",
    //     machine_id: "DSF0375",
    //     part_no: $("#partNum").val(),
    //     activity_type_id: 1003,
    //     reason_id: $("pending_opreason").val()
    //   });
    //   $.ajax({
    //       url: "/safran-iot/api/pending/start",
    //       type: "POST",
    //       contentType: "application/json",
    //       data: dataJSON,
    //       beforeSend: function () {
    //           $("#pend_startBtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
    //       },
    //       success: function (data)
    //       {
           
    //         window.location = "/finish";
    //         $("#pend_startBtn span").html("");
    //       },
    //       error: function (jqXhr, textStatus, errorThrown) {
    //           $("#pend_startBtn span").html("");
    //           $("#pend_startBtn").prop("disabled", false);
    //       },
    //   });
        
    // });
          
      
  });
  