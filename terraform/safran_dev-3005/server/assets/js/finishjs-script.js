jQuery(document).ready(function($){
    var totalSeconds = 0;
    var set_oneTime=true;
    function countTimer(time) {
        if(arguments.length)
        {
            var hms = time   // your input string
            var a = hms.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            totalSeconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
            ++totalSeconds;
            
        }
        else{
           
            ++totalSeconds;
        }
       
        var hour = Math.floor(totalSeconds /3600);
        var minute = Math.floor((totalSeconds - hour*3600)/60);
        var seconds = totalSeconds - (hour*3600 + minute*60);
        if(hour < 10)
            hour = "0"+hour;
        if(minute < 10)
            minute = "0"+minute;
        if(seconds < 10)
            seconds = "0"+seconds;
            if ($("#timer").exists()) {
           
                document.getElementById("timer").innerHTML = hour + ":" + minute + ":" + seconds;
                localStorage.setItem("FinishTime", hour + ":" + minute + ":" + seconds);
        }
    }
    if (localStorage.getItem("FinishTime") != null) {
        set_oneTime=false
        countTimer(localStorage.getItem("FinishTime"));
    }
    else{
        set_oneTime=true
        countTimer();
    }
    var timerVar=setInterval(countTimer, 1000);
	
    $(document).on("click", "#outoforder_finish_subbtn", function () {
        if ($("#outoford_form").valid()) {
            var dataJSON = JSON.stringify({
                reason_id: $("#outoford_op_reason").val()
            });
            clearInterval(timerVar);
            localStorage.clear();
            $("#outoforder_finish_subbtn").prop("disabled", true);
            var finishUrl="/safran-iot/api/outoforder/end";
            if(!set_oneTime){
                finishUrl="/safran-iot/api/unfinishedtask/end"
            }
            $.ajax({
                url:finishUrl,
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#outoforder_finish_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
                },
                success: function (data)
                {
                    $("#outoforder_finish_subbtn span").html("");
                    window.location = "/menu";
                    
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#outoforder_finish_subbtn span").html("");
                    $("#outoforder_finish_subbtn").prop("disabled", false);
                },
            });
        }
        
    });
    $(document).on("click", "#mainten_finish_subbtn", function () {
        if ($("#mainten_form").valid()) {
            var dataJSON = JSON.stringify({
                reason_id: $("#mainten_op_reason").val()
            });
            clearInterval(timerVar);
            localStorage.clear();
            $("#mainten_finish_subbtn").prop("disabled", true);
            var finishUrl="/safran-iot/api/maintenance/end";
            if(!set_oneTime){
                finishUrl="/safran-iot/api/unfinishedtask/end"
            }
            $.ajax({
                url: finishUrl,
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#mainten_finish_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
                },
                success: function (data)
                {
                    $("#mainten_finish_subbtn span").html("");
                    window.location = "/menu";
                    
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#mainten_finish_subbtn span").html("");
                    $("#mainten_finish_subbtn").prop("disabled", false);
                },
            });
        }
        
    });
    $(document).on("click", "#replace_finish_subbtn", function () {
        var dataJSON = JSON.stringify({
            description:$("#replaceTool_comment").val()
        });
        clearInterval(timerVar);
        localStorage.clear();
        $("#replace_finish_subbtn").prop("disabled", true);
        var finishUrl="/safran-iot/api/replacetool/end";
        if(!set_oneTime){
            finishUrl="/safran-iot/api/unfinishedtask/end"
        }
        $.ajax({
            url: finishUrl,
            type: "POST",
            data: dataJSON,
            contentType: "application/json",
            beforeSend: function () {
                $("#replace_finish_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                $("#replace_finish_subbtn span").html("");
                window.location = "/menu";
                
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#replace_finish_subbtn span").html("");
                $("#replace_finish_subbtn").prop("disabled", false);
            },
        });
        
    });
    $(document).on("click", "#pend_finish_subbtn", function () {
        if ($("#pending_form").valid()) {
            clearInterval(timerVar);
            localStorage.clear();
            var dataJSON = JSON.stringify({
                reason_id: $("#pending_opreason").val()
            });
            $("#pend_finish_subbtn").prop("disabled", true);
            var finishUrl="/safran-iot/api/pending/end";
            if(!set_oneTime){
                finishUrl="/safran-iot/api/unfinishedtask/end"
            }
            $.ajax({
                url: finishUrl,
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#pend_finish_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
                },
                success: function (data)
                {
                    $("#pend_finish_subbtn span").html("");
                    window.location = "/menu";
                    
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#pend_finish_subbtn span").html("");
                    $("#pend_finish_subbtn").prop("disabled", false);
                },
            });
        }
        
    });

    //var timerVar = setInterval(countTimer(""), 1000);
    // if (localStorage.getItem("FinishTitle") != null) {
    //     $("#finish_title").text(localStorage.getItem("FinishTitle"));
    //     $("#finishbtn_block").empty();
    //     var btnID="";
    //     if(localStorage.getItem("FinishTitle")=="Out of Order")
    //     {
    //         btnID="outoforder_finish_subbtn";
    //     }
    //     else if(localStorage.getItem("FinishTitle")=="Maintenance")
    //     {
    //         btnID="mainten_finish_subbtn";
    //     }
    //     else if(localStorage.getItem("FinishTitle")=="Pending")
    //     {
    //         btnID="pend_finish_subbtn";
    //         localStorage.setItem("FinishTitle", "Pending");
    //     }
    //     else{
    //         $("#finish_title").text("Replace Tool");
    //         btnID="replace_finish_subbtn";
    //     }
	// 	$("#finishbtn_block").html('<button id="'+btnID+'" class="btn btn-lg btn-primary" type="button">Finish<span></span></button>');
	// }
    // else{
    //     $("#finishbtn_block").empty();
    //     $("#finish_title").text("Replace Tool");
    //     btnID="replace_finish_subbtn";
    //     $("#finishbtn_block").html('<button id="'+btnID+'" class="btn btn-lg btn-primary" type="button">Finish<span></span></button>');
    // }
});