jQuery(document).ready(function($){
    function quality_dropdown(id)
    {
        $("#"+id).select2({
            placeholder: "Select",
            width: '100%'
        });
        $("#"+id).empty();
        $("#"+id).append('<option value="null" disabled="disabled">Data loading...</option>');
        $.ajax({
            url: "safran-iot/api/partmenu/qualityreason",
            type: "GET",
            contentType: "application/json",
            beforeSend: function () {
                $("#"+id).empty();
            },
            success: function (data)
            {
                $("#"+id).select2('close');
                $("#"+id).empty();
                $("#"+id).append('<option  value=""></option>');
                $.each(data, function(key, entry) {
                    if(entry.quality_reason_id)
                    {
                        $("#"+id).append('<option  value="' +entry.quality_reason_id+'">' + entry.quality_reason + '</option>');
                    }
                    
                });
            
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#"+id).append('<option value="null" disabled="disabled">Data can not be load!</option>');
            },
        });
    
    }
    $('#unload_check').on('click', function (e) {
        if($('input[name="unload-check"]').is(":checked"))
        {
            $("#num_gdpart").prop("disabled", false);
           
        }
        else{
            $("#num_gdpart").prop("disabled", true);
           
        }
    });
    var x=1
    var badPartNum=0
    var goodPart=0;
    $('#PC_finishpartBtn').on('click', function (e) {
        if($("#num_gdpart").val()!="")
        {
            x=1
            goodPart=$("#num_gdpart").val();
            var quantityReq=parseInt($("#num_quantity").val());
            //var quantityReq=5;
            console.log(quantityReq);
            console.log(goodPart);
            if(goodPart<0)
            {
                toastalert.error("Please enter the vaild number");
            }
            else if(goodPart>quantityReq)
            {
                toastalert.error("Good part is greater than the quantity");
            }
            else if(goodPart<quantityReq)
            {
                x=1;
                $("#quality_popup").modal('show');
                quality_dropdown("qualityReason_1");
                $('.append-dropdoen-block').remove();
                if($(".addMoreActivity-btn").length==0)
                {
                    $("#more_actbtn").append('<a href="javascript: void(0);"'+
                    'class="addMoreActivity-btn pull-right  flat-color-2 mt-2 mb-2 mt fa-lg"'+
                    'title="Add More "><i class="fa fa-plus-circle "'+
                        'aria-hidden="true" ></i></a>')
                }
                badPartNum = quantityReq-goodPart;
                if(x==badPartNum)
                {
                    $(".addMoreActivity-btn").remove();
                    $("#quality_subbtn").prop('disabled', false); 
                }
                
            }
            else
            {
                 $("#PC_finishpartBtn").prop("disabled", false);
            var dataJSON = JSON.stringify({
               site_id :"1",
                machine_id: "DSF0375",
                good_part:goodPart
            });
            $.ajax({
                url: "/safran-iot/api/partmenu/finish",
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#PC_finishpartBtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
                },
                success: function (data)
                {
                    localStorage.clear();
                    window.location = "/menu";
                    $("#PC_finishpartBtn span").html("");
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#PC_finishpartBtn span").html("");
                    $("#PC_finishpartBtn").prop("disabled", false);
                },
            });
            }
        }
        
        else{
            toastalert.error("Please enter the number of good parts");
            // $("#PC_finishpartBtn").prop("disabled", false);
            // var dataJSON = JSON.stringify({
            //    site_id :"1",
            //     machine_id: "DSF0375",
            //     good_part:1
            // });
            // $.ajax({
            //     url: "/safran-iot/api/partmenu/finish",
            //     type: "POST",
            //     contentType: "application/json",
            //     data: dataJSON,
            //     beforeSend: function () {
            //         $("#PC_finishpartBtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            //     },
            //     success: function (data)
            //     {
            //         window.location = "/menu";
            //         $("#PC_finishpartBtn span").html("");
            //     },
            //     error: function (jqXhr, textStatus, errorThrown) {
            //         $("#PC_finishpartBtn span").html("");
            //         $("#PC_finishpartBtn").prop("disabled", false);
            //     },
            // });
        }
         
       
    });
    $(document).on("click", ".addMoreActivity-btn", function () {
        $(this).remove();
        if (x < badPartNum) {
            x++;
            var field_HTML = 
            '<div class="row append-dropdoen-block">'+
            '<div class="col-lg-11">'+
                '<div class="form-floating mb-2 ">'+
                    '<select class="form-select mb-3" id="qualityReason_'+x+'" name="qualityReason[]">'+
                    '<option value=""></option>'+
                        
                    '</select>'+
                    '<label>Select Reason for discrepancy ' + x + ' </label>'+
                '</div>'+
            '</div>'  +
            '<div class="col-lg-1">'+
            '<a href="javascript: void(0); "class="addMoreActivity-btn pull-right  flat-color-2 mt-2 '+
            'mb-2 mt fa-lg "title="Add More "><i class="fa fa-plus-circle "aria-hidden="true" ></i></a>'+
            '</div>'+
            '</div>'
            $('#quality_form').append(field_HTML);
            quality_dropdown("qualityReason_"+x);
            if (x == badPartNum) {
                $(".addMoreActivity-btn").remove();
                $("#quality_subbtn").prop('disabled', false);  //button enabled
            }
        }
        
    });
    $('#quality_subbtn').on('click', function (e) {
        $("#quality_subbtn").prop("disabled", true);
        var qualityReason= $("[name='qualityReason[]']").map(function(){        
         return $(this).val();				
        }).get();	
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            good_part:goodPart,
            quality_reason_ids: qualityReason.join()
        });
        $.ajax({
            url: "safran-iot/api/partmenu/quality",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#quality_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                localStorage.clear();
                window.location = "/menu";
                $("#quality_subbtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#quality_subbtn span").html("");
                $("#quality_subbtn").prop("disabled", false);
            },
        });
    });
    $('#pc_returnOp').on('click', function (e) {
        $("#pc_returnOp").prop("disabled", true);
        $.ajax({
            url: "/safran-iot/api/partmenu/returnoperation",
            type: "POST",
            contentType: "application/json",
            beforeSend: function () {
                $("#pc_returnOp span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                localStorage.clear();
                window.location = "/part-menu-return";
                $("#pc_returnOp span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#pc_returnOp span").html("");
                $("#pc_returnOp").prop("disabled", false);
            },
        });
    });
    $('#pc_repeatOp').on('click', function (e) {
        window.location = "/part-menu";
     
    });
        
            

  

  });
  