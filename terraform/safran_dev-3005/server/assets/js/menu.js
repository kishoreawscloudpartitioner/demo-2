jQuery(document).ready(function ($) {
    $.ajax({
        url: "safran-iot/api/unfinishedtask/start",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            machine_id: "DSF0375"
        }),
        success: function (data, statusText, xhr) {
            if (xhr.status != 200) {
               
                if (data.activity_type_id == "1001") {
                    localStorage.setItem("FinishTitle", "Start Part");
                    localStorage.setItem("FinishTime", data.resume_time);
                    window.location = "/finish";
                }
                else if (data.activity_type_id == "1002") {
                    localStorage.setItem("FinishTitle", "Replace Tool");
                    localStorage.setItem("FinishTime", data.resume_time);
                    window.location = "/replace-tool";
                }
                else if (data.activity_type_id == "1003") {
                    localStorage.setItem("FinishTitle", "Pending");
                    localStorage.setItem("FinishTime", data.resume_time);
                    window.location = "/pending";
                }
                else if (data.activity_type_id == "1004") {
                    localStorage.setItem("FinishTitle", "Maintenance");
                    localStorage.setItem("FinishTime", data.resume_time);
                    window.location = "/maintenance";
                }
                else if (data.activity_type_id == "1005") {
                    localStorage.setItem("FinishTitle", "Out of Order");
                    localStorage.setItem("FinishTime", data.resume_time);
                    window.location = "/out-of-order";
                }
            }

        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log("2 call Errror");
            toastalert.error(jqXhr.responseJSON.message);
        },
    });
});    