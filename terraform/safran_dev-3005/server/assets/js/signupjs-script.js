jQuery(document).ready(function($){
    //$.validator_formID("login_form");
    $('#register_subbtn').on('click', function (e) {
        //$("#login_form").valid()
        if (true) {
            $("#register_subbtn").prop("disabled", true);
            localStorage.clear();
            var dataJSON = JSON.stringify({
                sign_id: $("#signup_operatorID").val(),
                first_name: $("#signup_fname").val(),
                last_name: $("#signup_lname").val(),
                email: $("#signup_email").val(),
                phone_no: $("#signup_phonenum").val(),
                role_id: $('input[name="role-id"]:checked').val(),
                password: $("#signup_pwd").val(),
            });
            $.ajax({
                url: "/safran-iot/api/signup",
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#register_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
                },
                success: function (data)
                {
                    window.location = "/login";
                    $("#register_subbtn span").html("");
                
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#register_subbtn").prop("disabled", false);
                },
            });
         }
    });

});