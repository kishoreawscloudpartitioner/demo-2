jQuery(document).ready(function($){
    var totalSeconds = 0;
    var set_oneTime=true;
    function countTimer(time) {
        if(arguments.length)
        {
            var hms = time   // your input string
            var a = hms.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            totalSeconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
            ++totalSeconds;
            
        }
        else{
           
            ++totalSeconds;
        }
       
        var hour = Math.floor(totalSeconds /3600);
        var minute = Math.floor((totalSeconds - hour*3600)/60);
        var seconds = totalSeconds - (hour*3600 + minute*60);
        if(hour < 10)
            hour = "0"+hour;
        if(minute < 10)
            minute = "0"+minute;
        if(seconds < 10)
            seconds = "0"+seconds;
            if ($("#timer").exists()) {
           
                document.getElementById("timer").innerHTML = hour + ":" + minute + ":" + seconds;
        
        }
    }
   
    countTimer();
    var timerVar=setInterval(countTimer, 1000);
    $('#setupCom_subbtn').on('click', function (e) {
        e.preventDefault();
        clearInterval(timerVar);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
           
          });
        $.ajax({
            url: "/safran-iot/api/partmenu/completesetup",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#setupCom_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/part-in-progress";
                $("#setupCom_subbtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#setupCom_subbtn span").html("");
                $("#setupCom_subbtn").prop("disabled", false);
            },
        });
       
      });
      $('#loadStart_btn').on('click', function (e) {
        e.preventDefault();
        clearInterval(timerVar);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id:100002 
           
          });
          $.ajax({
              url: "/safran-iot/api/partmenu/load",
              type: "POST",
              contentType: "application/json",
              data: dataJSON,
              beforeSend: function () {
                  $("#loadStart_btn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
              },
              success: function (data)
              {
                window.location = "/load-page";
                $("#loadStart_btn span").html("");
              },
              error: function (jqXhr, textStatus, errorThrown) {
                $("#loadStart_btn span").html("");
                $("#loadStart_btn").prop("disabled", false);
              },
          });
       
      });
});