jQuery(document).ready(function($){
    var actId=100004;
    $('#admin_main_replaceChk').on('click', function (e) {
        if($('input[name="admin-main-replaceChk"]').is(":checked"))
        {
            actId=100003;
           
        }
        else{
            actId=100004;
           
        }
    });
    function format(d) {
        // `d` is the original data object for the row
        var gdpart=d.good_part?d.good_part:0;
        return (
            '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            '<tr>' +
            '<td>Work Order No:</td>' +
            '<td>' +
            d.work_order_no +
            '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Part Number:</td>' +
            '<td>' +
            d.part_no +
            '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Operation number:</td>' +
            '<td>' +
            d.operation_no +
            '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Start Time:</td>' +
            '<td>' +
            d.start_time +
            '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>End Time:</td>' +
            '<td>' +
            d.end_time +
            '</td>' +
            '</tr>' +
            '</table>'
        );
    }
    var table;
   function mainRunSerachTable(url,inputData)
   {
    
    table = $('#example').DataTable({
        searching: false,
        responsive: true,
        "ajax": {
            "url":url,
            "type": "post",
            "contentType":"application/json",
            data: function ( d ) {
                return inputData;
            },
            error: function (xhr, error, code)
            {
                console.log(xhr);
                toastalert.error(xhr.responseJSON[0].message);
                $(".dataTables_empty").empty().html("<span style='color:#ccc'>No data available</span>");
            }
        },
        columns: [
            {
                className:'dt-control',
                defaultContent:'',
                orderable: false,
                data: null,
            },
            { 
                data: null, 
                render: function ( data, type, row ) {
                     return 'Kirkland'
                }
            },
            { data: 'machine_id' },
            { data: 'operator_no' },
            { 
                data: null, 
                render: function ( data, type, row ) {
                     return '<div class="dropdown">'+
                    '<span  type="button" data-bs-toggle="dropdown" aria-expanded="false">'+
                      '<i class="fa fa-ellipsis-v" aria-hidden="true"></i>'+
                    '</span>'+
                    '<ul class="dropdown-menu custom-menu">'+
                     '<li><a class="dropdown-item edit" href="javascript:void(0);" id="'+data.id+'"><i class="fa fa-pencil me-2" aria-hidden="true"></i>Edit</a></li>'+
                      '<li><a class="dropdown-item delete" href="javascript:void(0);" id="'+data.id+'"><i class="fa fa-trash-o me-2" aria-hidden="true"></i>Delete</a></li>'+
                   '</ul>'+
                  '</div>'
                }
            },
        ],
        order: [[1, 'asc']],
    });
   }
   // Add event listener for opening and closing details
    $(document).on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
 
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
    $("#st_date")
    .datepicker({ todayHighlight: !0, autoclose: !0, orientation: "bottom right" })
    .on("changeDate", function () {
        $(this).addClass("has-val"), $("#end_date").datepicker("setStartDate", new Date($(this).val()));
    });
   
    $("#end_date")
    .datepicker({ todayHighlight: !0, autoclose: !0, orientation: "bottom right" })
    .on("changeDate", function () {
        $(this).addClass("has-val"), $("#st_date").datepicker("setEndDate", new Date($(this).val()));
    });
   
    $.validator_formID("main_run_serach");
    $('#main_run_serachbtn').on('click', function (e) {
        if($("#main_run_serach").valid())
        {
            $("#main_run_serach").hide();
            $('.search-table').removeClass('d-none');
            $("#main_run_serachbtn").prop("disabled", false);
           
            var inputData = JSON.stringify({
                activity_type_id: actId,
                start_date: $("#st_date").val(),
                end_date: $("#end_date").val(),
                machine_id: $("#search_mc_ID").val(),
                operator_no: $("#search_op").val(),
                workorder_no: $("#search_wrk_ord").val(),
                site_id:$("#search_site_name").val()
            });
            mainRunSerachTable("/safran-iot/api/admin/maintainpart/pause/getdata",inputData)
        }
    });

    //edit
   function timepickercal(txt1, txt2){
		var fromTimeid = $('#' + txt1);
		var toTimeid = $('#' + txt2);
		//Selecting Time
		fromTimeid.datetimepicker({
            format: 'HH:mm',
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-chevron-up",
				down: "fa fa-chevron-down"
			},
			widgetPositioning: {
				vertical: 'bottom'
			}			 
		});
		toTimeid.datetimepicker({
            format: 'HH:mm',
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-chevron-up",
				down: "fa fa-chevron-down"
			},
			widgetPositioning: {
				vertical: 'bottom'
			}          
		});
		
	}
    var editID;
    $(document).on('click', '.edit', function () {
        editID=$(this).attr('id');
        $('.modal-container').load("/edit-popup",function(result) {
			
            $("#edit-popup").modal('show');
            var dataJSON = JSON.stringify({
                id:editID,
                activity_type_id: actId
            });
            $.ajax({
                url: "/safran-iot/api/admin/maintainpart/pause/singledata",
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                success: function (data)
                {
                    $("#edit_GP").val(data.good_part);
                    $("#edit_SD").val(data.start_date);
                    $("#edit_ED").val(data.end_date);
                    $("#edit_ST").val(data.start_time);
                    $("#edit_ET").val(data.end_time);
                    $(".loading-bg").hide();
                 
                    
                },
                error: function (jqXhr, textStatus, errorThrown) {
                   
                },
            });
           
			$("#edit_SD")
            .datepicker({ todayHighlight: !0, autoclose: !0, orientation: "bottom right" })
            .on("changeDate", function () {
                $(this).addClass("has-val"), $("#edit_ED").datepicker("setStartDate", new Date($(this).val()));
            });
        
            $("#edit_ED")
            .datepicker({ todayHighlight: !0, autoclose: !0, orientation: "bottom right" })
            .on("changeDate", function () {
                $(this).addClass("has-val"), $("#edit_SD").datepicker("setEndDate", new Date($(this).val()));
            });
			timepickercal("edit_ST", "edit_ET");
        });
    });
    $(document).on("click", "#update_subbtn", function () {
        $("#update_subbtn").prop("disabled", true);
        var goodPart=$("#edit_GP").val()?$("#edit_GP").val():"0"
        var inputData = JSON.stringify({
            id: editID,
            activity_type_id: actId,
            start_time: $("#edit_SD").val()+"T"+$("#edit_ST").val()+":00",
            end_time:$("#edit_ED").val()+"T"+$("#edit_ET").val()+":00",
        });
       
        $.ajax({
            url: "safran-iot/api/admin/maintainpart/pause/update",
            type: "POST",
            contentType: "application/json",
            data: inputData,
            beforeSend: function () {
                $("#update_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                $("#edit-popup").modal('hide');	
                $("#update_subbtn span").html("");
                table.ajax.reload();
                
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#update_subbtn span").html("");
                $("#update_subbtn").prop("disabled", false);
            },
        });
        
    });
    var deleteID;
    $(document).on('click', '.delete', function () {
       deleteID=$(this).attr('id');
        $('.modal-container').load("/delete-popup",function(result) {
			
            $("#delete_popup").modal('show');
            
        });
    });
    $(document).on("click", "#delete_subbtn", function () {
        $("#delete_subbtn").prop("disabled", true);
        var inputData = JSON.stringify({
            id: deleteID,
            activity_type_id: actId,
          
        });
       
        $.ajax({
            url: "/safran-iot/api/admin/maintainpart/pause/delete",
            type: "POST",
            contentType: "application/json",
            data: inputData,
            beforeSend: function () {
                $("#delete_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                $("#delete_popup").modal('hide');	
                $("#delete_subbtn span").html("");
                table.ajax.reload();
                
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#delete_subbtn span").html("");
                $("#delete_subbtn").prop("disabled", false);
            },
        });
        
    });
    
});