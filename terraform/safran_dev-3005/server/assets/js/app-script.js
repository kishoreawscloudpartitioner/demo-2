jQuery(document).ready(function($){
    var e = { enableSounds: !1,
        autoClose: true,
        progressBar: true, };
    window.toastalert = new Toasty(e);
    toastalert.configure(e);
    
    jQuery.validator.addMethod(
        "passwordCheck",
        function (t, e, a) {
            return !!this.optional(e) || (!!/[A-Z]/.test(t) && !!/[a-z]/.test(t) && !!/[0-9]/.test(t) && !!/[$&+,:;=?@#|<>.^*()%!-]/.test(t));
        },
        "Password must contain atleast one uppercase, one lowercase, one number and special character."
    );
    jQuery.validator.addMethod(
        "numCheck",
        function (t, e) {
            return this.optional(e) || /^\d{10}$/i.test(t);
        },
        "Please enter valid number."
    );
    $.validator_formID = function (t) {
        $("#" + t).validate({
            ignore: "",
            errorClass: "input-error",
            errorElement: "div",
            errorPlacement: function (t, e) {
                "checkbox" == e.attr("type") ? t.insertBefore("ul.list-group") : e.next(".select2-container").length ? t.insertAfter(e.next(".select2-container")) : t.insertAfter(e);
            },
            
        });
    };
	$("#frg-pwd-btn1").click(function(){
		$("#frg-pwd-block1").addClass("d-none");
		
		$("#frg-pwd-block2").removeClass("d-none");
		//$("#frg-pwd-block2").addClass("animate__delay-2s");
		$("#frg-pwd-block2").addClass("animate__fadeInRight");
		
	});
	$("#frg-pwd-btn2").click(function(){
		$("#frg-pwd-block2").addClass("d-none");
		
		$("#frg-pwd-block3").removeClass("d-none");
		//$("#frg-pwd-block2").addClass("animate__delay-2s");
		$("#frg-pwd-block3").addClass("animate__fadeInRight");
		
	});
	var pathname = window.location.pathname;
	if(pathname.includes("out-of-order"))
	{
		localStorage.setItem("FinishTitle", "Out of Order");
	}
	else if(pathname.includes("maintenance"))
	{
		localStorage.setItem("FinishTitle", "Maintenance");
	}
	else if(pathname.includes("pending"))
	{
		localStorage.setItem("FinishTitle", "Pending");
	}
   
    $('#replace_tool_cont').on('click', function (e) {
        
        $("#replace_tool_cont").prop("disabled", true);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
        });
        $.ajax({
            url: "safran-iot/api/replacetool/start",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#replace_tool_cont span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/finish";
                $("#replace_tool_cont span").html("");
                
            
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#replace_tool_cont").prop("disabled", false);
            },
        });
       
    });
    //OUT of order
    $('#outoford_startbtn').on('click', function (e) {
        $("#outoford_subbtn").prop("disabled", true);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id:"1005",
        });
        $.ajax({
            url: "safran-iot/api/outoforder/start",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#outoford_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/out-of-order";
                $("#outoford_subbtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#outoford_subbtn span").html("");
                $("#outoford_subbtn").prop("disabled", false);
            },
        });
    });

    $('#mainten_startbtn').on('click', function (e) {
        $("#mainten_startbtn").prop("disabled", true);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
            activity_type_id:"1004",
        });
        $.ajax({
            url: "safran-iot/api/maintenance/start",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#mainten_startbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/maintenance";
                $("#mainten_startbtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#mainten_startbtn span").html("");
                $("#mainten_startbtn").prop("disabled", false);
            },
        });
      
    });

    $('#replaceTool_startbtn').on('click', function (e) {
        $("#replaceTool_startbtn").prop("disabled", true);
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
        });
        $.ajax({
            url: "/safran-iot/api/replacetool/start",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#replaceTool_startbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/replace-tool";
                $("#replaceTool_startbtn span").html("");
                
            
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#replaceTool_startbtn").prop("disabled", false);
            },
        });
    });
    $('#pend_startBtn').on('click', function (e) {
        e.preventDefault();
        $("#pend_startBtn").prop("disabled", true);
        var dataJSON = JSON.stringify({
          machine_id: "DSF0375",
          activity_type_id: 1003,
        });
        $.ajax({
            url: "/safran-iot/api/pending/start",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#pend_startBtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
             
              window.location = "/pending";
              $("#pend_startBtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#pend_startBtn span").html("");
                $("#pend_startBtn").prop("disabled", false);
            },
        });
          
      });

      /* Site DropDown */
      function site_dropdown(id,url)
      {
          if ($("#"+id).exists()) {
              $("#"+id).select2({
                  placeholder: "Select",
              });
              $("#"+id).empty();
              $("#"+id).append('<option value="null" disabled="disabled">Data loading...</option>');
             
              $.ajax({
                  url: url,
                  type: "GET",
                  contentType: "application/json",
                  beforeSend: function () {
                      $("#"+id).empty();
                  },
                  success: function (data)
                  {
                      $("#"+id).select2('close');
                      $("#"+id).empty();
                      $("#"+id).append('<option  value=""></option>');
                      $.each(data, function(key, entry) {
                       
                        $("#"+id).append('<option  value="' + entry.site_id +'">' + entry.site + '</option>');
                         
                          
                      });
                  
                  },
                  error: function (jqXhr, textStatus, errorThrown) {
                      $("#"+id).append('<option value="null" disabled="disabled">Data can not be load!</option>');
                  },
              });
          }
      }
      
      site_dropdown("search_site_name","/safran-iot/api/site/dropdown");

      
    // window.addEventListener('beforeunload', function (e) {
    //     e.preventDefault();
    //     if(e.returnValue)
    //     {
    //         $.ajax({
    //             url: "/safran-iot/api/outoforder/operatordropdown",
    //             type: "GET",
    //             contentType: "application/json",
    //             beforeSend: function () {
    //                 $("#outoford_op_reason").empty();
    //             },
    //             success: function (data)
    //             {
    //                 $("#outoford_op_reason").select2('close');
    //                 $("#outoford_op_reason").empty();
    //                 $("#outoford_op_reason").append('<option  value=""></option>');
    //                 $.each(data, function(key, entry) {
    //                     if(entry.reason)
    //                     {
    //                         $("#outoford_op_reason").append('<option  value="' + entry.reason_id +'-'+entry.activity_type+'">' + entry.reason + '</option>');
    //                     }
                        
    //                 });
    //                 e.returnValue = '';
    //             },
    //             error: function (jqXhr, textStatus, errorThrown) {
    //                 $("#outoford_op_reason").append('<option value="null" disabled="disabled">Data can not be load!</option>');
    //             },
    //         });
    //     }
    //     else
    //     {
    //         e.returnValue = '';
    //     }
        
    // });
});