jQuery(document).ready(function($){
    
    $.validator_formID("mainten_form");
    function maintenance_dropdown(id,url,reason)
    {
        if ($("#"+id).exists()) {
            $("#"+id).select2({
                placeholder: "Select",
            });
            $("#"+id).empty();
            $("#"+id).append('<option value="null" disabled="disabled">Data loading...</option>');
            var dataJSON = JSON.stringify({
                machine_type: "3 Axis Router",
                activity_type_id: "1004",
                //reason_for: reason
            });
            $.ajax({
                url: url,
                type: "POST",
                contentType: "application/json",
                data: dataJSON,
                beforeSend: function () {
                    $("#"+id).empty();
                },
                success: function (data)
                {
                    $("#"+id).select2('close');
                    $("#"+id).empty();
                    $("#"+id).append('<option  value=""></option>');
                    $.each(data, function(key, entry) {
                        if(entry.reason)
                        {
                            $("#"+id).append('<option  value="'+entry.reason_id+'">' + entry.reason + '</option>');
                        }
                        
                    });
                
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    $("#"+id).append('<option value="null" disabled="disabled">Data can not be load!</option>');
                },
            });
        }
    }
    maintenance_dropdown("mainten_op_reason","/safran-iot/api/maintenance/dropdown","Operator");
    maintenance_dropdown("mainten_maint_reason","/safran-iot/api/maintenance/dropdown","Maintenance");
    // $('#mainten_subbtn').on('click', function (e) {
    //     if ($("#mainten_form").valid()) {
    //         $("#mainten_subbtn").prop("disabled", true);
    //         var all_Id;
    //         if ($("#mainten_op_reason").exists())
    //         {
    //             all_Id=$("#mainten_op_reason").val().split("-");
    //         }
    //         else
    //         {
    //             all_Id=$("#mainten_maint_reason").val().split("-");
    //         }
           
    //         var dataJSON = JSON.stringify({
    //            site_id :"1",
    //             machine_id: "DSF0375",
    //             activity_type_id:all_Id[1],
    //             reason_id: all_Id[0]
    //         });
    //         window.location = "/menu";
    //         // $.ajax({
    //         //     url: "safran-iot/api/maintenance/start",
    //         //     type: "POST",
    //         //     contentType: "application/json",
    //         //     data: dataJSON,
    //         //     beforeSend: function () {
    //         //         $("#mainten_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
    //         //     },
    //         //     success: function (data)
    //         //     {
    //         //         //localStorage.setItem("User-ID",data.sign_id);
    //         //         window.location = "/finish";
    //         //         $("#mainten_subbtn span").html("");
                    
                
    //         //     },
    //         //     error: function (jqXhr, textStatus, errorThrown) {
    //         //         $("#mainten_subbtn span").html("");
    //         //         $("#mainten_subbtn").prop("disabled", false);
    //         //     },
    //         // });
    //      }
    // });

});