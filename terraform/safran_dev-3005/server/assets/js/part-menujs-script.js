jQuery(document).ready(function($){
  $('#PS_opCode').select2({
      placeholder: "Select",
    });
    if(localStorage.getItem("opNum"))
    {
      $('#PS_opCode').val(localStorage.getItem("opNum"));
      $('#PS_opCode').trigger('change');
      opCode=$('#PS_opCode').val();
      $('#opNum').val(opCode.slice(opCode.length - 4));
      var WrkOpNum=opCode.slice(opCode.length - 11);
      $('#wrkorder').val(WrkOpNum.slice(0,-4));
      $("#pm_startBtn").prop("disabled", false);
      //remaming data fill
      var dataJSON = JSON.stringify({
        workorder_no: $('#wrkorder').val(),
        operation_no: $('#opNum').val(),
    });
      $.ajax({
          url: "/safran-iot/api/partmenu",
          type: "POST",
          contentType: "application/json",
          data: dataJSON,
          success: function (data)
          {
            $("#partNum").val(data.part_no),
            $("#partDesc").val(data.part_desc),
            $("#opDesc").val(data.operation_desc),
            $("#numPart_toProd").val(data.quantity)
            $("#pm_startBtn").prop("disabled", false);
          
          },
          error: function (jqXhr, textStatus, errorThrown) {
              $("#pm_startBtn").prop("disabled", true);
          },
      });
      
    }
    var opCode;
    $(document).on("change", "#PS_opCode", function () {
       opCode=$('#PS_opCode').val();
        $('#opNum').val(opCode.slice(opCode.length - 4));
        var WrkOpNum=opCode.slice(opCode.length - 11);
        $('#wrkorder').val(WrkOpNum.slice(0,-4));
        $("#pm_startBtn").prop("disabled", false);
        //remaming data fill
        var dataJSON = JSON.stringify({
          workorder_no: $('#wrkorder').val(),
          operation_no: $('#opNum').val(),
      });
        $.ajax({
            url: "/safran-iot/api/partmenu",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            success: function (data)
            {
              $("#partNum").val(data.part_no),
              $("#partDesc").val(data.part_desc),
              $("#opDesc").val(data.operation_desc),
              $("#numPart_toProd").val(data.quantity)
              $("#pm_startBtn").prop("disabled", false);
            
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#pm_startBtn").prop("disabled", true);
            },
        });
        
    });
    

     
      $('#pm_startBtn').on('click', function (e) {
        e.preventDefault();
        var reqtype=100005
        if($('input[name="pm-reqtype"]').is(":checked"))
        {
          reqtype=$('input[name="pm-reqtype"]:checked').val();
           
        }
        
        $("#pm_startBtn").prop("disabled", true);
        var dataJSON = JSON.stringify({
          workorder_no: $('#wrkorder').val(),
          operation_no: $('#opNum').val(),
         site_id :"1",
          machine_id: "DSF0375",
          activity_type_id: reqtype,
          quantity: $("#numPart_toProd").val()
         
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/start",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#pm_startBtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
              if(reqtype==100002)
              {
                window.location = "/load-page";
              }
              else if(reqtype==100005){
                window.location = "/part-in-progress";
              }
              else{
                window.location = "/setup-page";
              }
              localStorage.setItem("opNum", opCode);
                $("#pm_startBtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#pm_startBtn span").html("");
                $("#pm_startBtn").prop("disabled", false);
            },
        });
          
      });
        
    
});
