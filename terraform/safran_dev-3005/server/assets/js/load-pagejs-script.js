jQuery(document).ready(function($){
    var totalSeconds = 0;
    function countTimer(time) {
        if(arguments.length)
        {
            var hms = time   // your input string
            var a = hms.split(':'); // split it at the colons

            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            totalSeconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
            ++totalSeconds;
            
        }
        else{
           
            ++totalSeconds;
        }
       
        var hour = Math.floor(totalSeconds /3600);
        var minute = Math.floor((totalSeconds - hour*3600)/60);
        var seconds = totalSeconds - (hour*3600 + minute*60);
        if(hour < 10)
            hour = "0"+hour;
        if(minute < 10)
            minute = "0"+minute;
        if(seconds < 10)
            seconds = "0"+seconds;
            if ($("#timer").exists()) {
           
                document.getElementById("timer").innerHTML = hour + ":" + minute + ":" + seconds;
        
        }
    }
   
    countTimer();
    var timerVar=setInterval(countTimer, 1000);
    $('#loadCom_subbtn').on('click', function (e) {
        e.preventDefault();
        clearInterval(timerVar);
        localStorage.clear();
        var dataJSON = JSON.stringify({
           site_id :"1",
            machine_id: "DSF0375",
        });
        $.ajax({
            url: "/safran-iot/api/partmenu/completesetup",
            type: "POST",
            contentType: "application/json",
            data: dataJSON,
            beforeSend: function () {
                $("#loadCom_subbtn span").html("<i class='p-0 ms-2 fa fa-spinner fa-spin'></i>");
            },
            success: function (data)
            {
                window.location = "/part-in-progress";
                $("#loadCom_subbtn span").html("");
            },
            error: function (jqXhr, textStatus, errorThrown) {
                $("#loadCom_subbtn span").html("");
                $("#loadCom_subbtn").prop("disabled", false);
            },
        });
       
      });
});