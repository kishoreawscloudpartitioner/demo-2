const Pool = require('pg').Pool

// const isProduction = process.env.NODE_ENV === 'production'

// const connectionString = `postgresql://${process.env.db_user}:${process.env.db_password}@${process.env.db_host}:${process.env.db_port}/${process.env.db_name}?schema=${process.env.db_schema}`

// const pool = new Pool({
//     connectionString: isProduction ? process.env.db_url : connectionString
// })

const pool = new Pool({
    user: process.env.db_user,
    host: process.env.db_host,
    database: process.env.db_name,
    password: process.env.db_password,
    port: process.env.db_port,
})

module.exports = pool