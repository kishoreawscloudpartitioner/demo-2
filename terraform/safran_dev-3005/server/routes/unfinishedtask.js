const express = require('express')
const unfinishedTaskController = require('../controller/unfinishedtask')

const router = express.Router()

router
    .post('/start', unfinishedTaskController.unfinishedTask)
    .post('/end', unfinishedTaskController.unfinishedTaskEnd)

module.exports = router