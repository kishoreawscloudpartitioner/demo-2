const express = require('express')
const outOfOrderController = require('../controller/outoforder')
const dropdownController = require('../controller/dropdown')

const router = express.Router()

router
    .post('/dropdown', dropdownController.dropdown)
    .post('/start', outOfOrderController.outoforderstart)
    .post('/end', outOfOrderController.outoforderend)

module.exports = router