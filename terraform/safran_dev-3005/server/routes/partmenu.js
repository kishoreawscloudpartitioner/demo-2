const express = require('express')
const partmenuController = require('../controller/partmenu')
const dropdownController = require('../controller/dropdown')

const router = express.Router()

router
    .post('/', partmenuController.partmenu)
    .post('/start', partmenuController.startPart)
    .post('/completesetup', partmenuController.completeSetup)
    .post('/load', partmenuController.load)
    .post('/finishpart', partmenuController.finishPart)
    .post('/pause', partmenuController.pause)
    .post('/returnoperation', partmenuController.returnOperation)
    .post('/finish', partmenuController.finish)
    .get('/qualityreason', dropdownController.qualityDropdown)
    .post('/quality', partmenuController.quality)

module.exports = router