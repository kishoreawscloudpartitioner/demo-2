const express = require('express')
const maintainpartRouter = require('./maintainpart')
const pendingRouter = require('./pending')
const replacetoolRouter = require('./replacetool')
const maintenanceRouter = require('./maintenance')
const outoforderRouter = require('./outoforder')

const router = express.Router()

router
    .use('/maintainpart', maintainpartRouter)
    .use('/pending', pendingRouter)
    .use('/replacetool', replacetoolRouter)
    .use('/maintenance', maintenanceRouter)
    .use('/outoforder', outoforderRouter)

module.exports = router