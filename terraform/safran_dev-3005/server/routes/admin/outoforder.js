const express = require('express')
const outoforderController = require('../../controller/admin/outoforder')

const router = express.Router()

router
    .post('/getdata', outoforderController.getData)
    .post('/singledata', outoforderController.singleData)
    .post('/update', outoforderController.update)
    .post('/delete', outoforderController.deleteOutOfOrder)

module.exports = router