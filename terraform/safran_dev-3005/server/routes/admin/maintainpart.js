const express = require('express')
const runtimeController = require('../../controller/admin/maintianpart/runtime')
const setuptimeController = require('../../controller/admin/maintianpart/setuptime')
const loadController = require('../../controller/admin/maintianpart/load')
const unloadController = require('../../controller/admin/maintianpart/unload')
const pauseController = require('../../controller/admin/maintianpart/pause')

const router = express.Router()

router
    .post('/runtime/getdata', runtimeController.getData)
    .post('/runtime/singledata', runtimeController.getSingleData)
    .post('/runtime/update', runtimeController.update)
    .post('/runtime/delete', runtimeController.delete_part)

    .post('/setuptime/getdata', setuptimeController.getData)
    .post('/setuptime/singledata', setuptimeController.getSingleData)
    .post('/setuptime/update', setuptimeController.update)
    .post('/setuptime/delete', setuptimeController.delete_setup)

    .post('/load/getdata', loadController.getData)
    .post('/load/singledata', loadController.getSingleData)
    .post('/load/update', loadController.update)
    .post('/load/delete', loadController.delete_load)

    .post('/unload/getdata', unloadController.getData)
    .post('/unload/singledata', unloadController.getSingleData)
    .post('/unload/update', unloadController.update)
    .post('/unload/delete', unloadController.delete_unload)
    
    .post('/pause/getdata', pauseController.getData)
    .post('/pause/singledata', pauseController.getSingleData)
    .post('/pause/update', pauseController.update)
    .post('/pause/delete', pauseController.delete_pause)

module.exports = router