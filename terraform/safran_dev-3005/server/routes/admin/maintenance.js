const express = require('express')
const maintenanceController = require('../../controller/admin/maintenance')

const router = express.Router()

router
    .post('/getdata', maintenanceController.getData)
    .post('/singledata', maintenanceController.singleData)
    .post('/update', maintenanceController.update)
    .post('/delete', maintenanceController.deleteMaintenance)

module.exports = router