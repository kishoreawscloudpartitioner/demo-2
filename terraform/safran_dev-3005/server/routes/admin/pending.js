const express = require('express')
const pendingController = require('../../controller/admin/pending')

const router = express.Router()

router
    .post('/getdata', pendingController.getData)
    .post('/singledata', pendingController.singleData)
    .post('/update', pendingController.update)
    .post('/delete', pendingController.deletePending)

module.exports = router