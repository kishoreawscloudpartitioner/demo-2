const express = require('express')
const replacetoolController = require('../../controller/admin/replacetool')

const router = express.Router()

router
    .post('/getdata', replacetoolController.getData)
    .post('/singledata', replacetoolController.singleData)
    .post('/update', replacetoolController.update)
    .post('/delete', replacetoolController.deleteReplaceTool)

module.exports = router