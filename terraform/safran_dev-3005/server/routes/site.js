const express = require('express')
const dropdownController = require('../controller/dropdown')

const router = express.Router()

router
    .get('/dropdown', dropdownController.siteDropdown)

module.exports = router