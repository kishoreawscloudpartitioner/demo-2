const express = require('express')
const replacetoolController = require('../controller/replacetool')

const router = express.Router()

router
    .post('/start', replacetoolController.replaceToolStart)
    .post('/end', replacetoolController.replaceToolEnd)

module.exports = router