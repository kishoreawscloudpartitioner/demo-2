const express = require('express')
const maintenanceController = require('../controller/maintenance')
const dropdownController = require('../controller/dropdown')

const router = express.Router()

router
    .post('/dropdown', dropdownController.dropdown)
    .post('/start', maintenanceController.maintenanceStart)
    .post('/end', maintenanceController.maintenanceEnd)

module.exports = router