const express = require('express')
const pendingController = require('../controller/pending')
const dropdownController = require('../controller/dropdown')

const router = express.Router()

router
    .post('/reasons', dropdownController.dropdown)
    .post('/start', pendingController.pendingStart)
    .post('/end', pendingController.pendingEnd)
    

module.exports = router