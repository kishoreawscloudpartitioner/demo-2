const express = require('express')

const router = express.Router()

router
    .get('/menu', (req, res) => {
        res.send('MENU PAGE')
    })
    .get('/outoforder', (req, res) => {
        res.send('OUT OF ORDER PAGE')
    })
module.exports = router