// Packages Imports
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
var session = require('express-session')
const flash = require('express-flash')
const dotenv = require('dotenv').config()

// Router Imports
const signupRouter = require('./routes/signup')
const loginRouter = require('./routes/login')
const outOfOrderRouter = require('./routes/outoforder')
const maintenanceRouter = require('./routes/maintenance')
const replaceToolRouter = require('./routes/replacetool')
const unfinishedTaskRouter = require('./routes/unfinishedtask')
const partMenuRouter = require('./routes/partmenu')
const pendingRouter = require('./routes/pending')
const adminRouter = require('./routes/admin/index')
const siteRouter = require('./routes/site')

const app = express();

//Fornt Start config -->
var path = require("path");
var exhbs = require("express-handlebars");
var hbs = exhbs.create({
    extname: "hbs",
    defaultLayout: "",
    layoutsDir: __dirname + "/views/",
    partialsDir: __dirname + "/views/",
    helpers: {
        cust_if: function (v1, operator, v2, options) {
          switch (operator) {
            case "==":
              return v1 == v2 ? options.fn(this) : options.inverse(this);
            case "===":
              return v1 === v2 ? options.fn(this) : options.inverse(this);
            case "!=":
              return v1 != v2 ? options.fn(this) : options.inverse(this);
            case "!==":
              return v1 !== v2 ? options.fn(this) : options.inverse(this);
            case "<":
              return v1 < v2 ? options.fn(this) : options.inverse(this);
            case "<=":
              return v1 <= v2 ? options.fn(this) : options.inverse(this);
            case ">":
              return v1 > v2 ? options.fn(this) : options.inverse(this);
            case ">=":
              return v1 >= v2 ? options.fn(this) : options.inverse(this);
            case "&&":
              return v1 && v2 ? options.fn(this) : options.inverse(this);
            case "||":
              return v1 || v2 ? options.fn(this) : options.inverse(this);
            default:
              return options.inverse(this);
          }
        },
    },
   
  });
  
app.engine("hbs", hbs.engine);

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");
app.use(express.static(path.join(__dirname, "assets")));
// End <----
const port = process.env.PORT || 3000

// All Middleware Defined 
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)
app.use(
    session({
        secret: "safran",
        resave: false,
        saveUninitialized: false
    })
)

app.use(flash())

// Authentication Middlewares
function checkAuth(req, res, next) {
    if (!req.session.is_logged_in) {
        return res.redirect('/login')
    }
    next()
}
function isAuth(req, res, next) {
    if (req.session.is_logged_in){
        return res.redirect('/menu')
    }
    next()
}
function checkAllOption(req, res, next) {
  if (req.session.role_id==101 || req.session.role_id==102) {
    req.session.set_all_option=true;
  }
  else
  {
    req.session.set_all_option=false;
  }
  next()
}


//fornt end website Route --->
app.get('/sign-up', isAuth,(req, res) => {
    res.render("sign-up");
});
app.get('/machineInfo', function(req, res){
  if (req.query.machineid && req.query.siteid) {
    req.session.mcID = req.query.machineid
    req.session.sitID = req.query.siteid
    res.redirect('/login')
  }
  // res.json({"machineID": req.query.machineid,"siteID":req.query.siteid})
});
app.get('/login', isAuth, (req, res) => {
    res.render("login");
})

app.get('/forgot-password', isAuth, (req, res) => {
    res.render("forgot-password");
});
app.get('/logout', (req, res) => {
    req.session.destroy()
    res.redirect('/login');
})
app.get('/', checkAuth, (req, res) => {
  if (req.session.user_id=="0000") {
     res.redirect('/admin-menu')
  }
  else if (req.session.user_id=="2222") {
    res.redirect('/mobile-menu')
  }
  else
  {
    res.redirect('/menu');
  }
    
})
app.get('/menu', checkAuth,checkAllOption,(req, res) => {
    if (req.session.user_id=="0000") {
      req.session.home_url="/admin-menu"
      res.redirect('/admin-menu')
  }
  else if (req.session.user_id=="2222") {
    res.redirect('/mobile-menu')
  }
  else
  {
    req.session.home_url="/menu"
    res.render("menu",{
      userInfo: req.session,
    });
  }
 
});
app.get('/mobile-menu', checkAuth,(req, res) => {
  res.render("mobile-menu",{
      userInfo: req.session
    });
});

app.get('/out-of-order', checkAuth,checkAllOption,(req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("out-of-order",{
        userInfo: req.session
      });
});
app.get('/maintenance',checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
  res.render("maintenance",{
      userInfo: req.session
    });
});
app.get('/replace-tool',checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
  res.render("replace-tool",{
      userInfo: req.session
    });
});
app.get('/pending', checkAuth,(req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("pending",{
        userInfo: req.session
      });
});

app.get('/finish', checkAuth,(req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("finish",{
        userInfo: req.session
      });
});
app.get('/part-menu', checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("part-menu",{
        userInfo: req.session
      });
});
app.get('/part-menu-return', checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
  res.render("part-menu-return",{
      userInfo: req.session
    });
});
app.get('/load-page',checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("load-page",{
      userInfo: req.session
    });
});
app.get('/part-in-progress', checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("part-in-progress",{
      userInfo: req.session
    });
});
app.get('/setup-page', checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
    res.render("setup-page",{
      userInfo: req.session
    });
});
app.get('/part-complete', checkAuth, (req, res) => {
  req.session.home_url="javascript:void(0)"
  res.render("part-complete",{
    userInfo: req.session
  });
});
app.get('/part-replace-pause-popup', (req, res) => {
  req.session.home_url="javascript:void(0)"
  res.render("part-replace-pause-popup");
});
app.get('/part-pause-popup', (req, res) => {
  req.session.home_url="javascript:void(0)"
  res.render("part-pause-popup");
});
app.get('/admin-menu', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-menu",{
    userInfo: req.session
  });
});
app.get('/admin-main-part', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-part",{
    userInfo: req.session
  });
});
app.get('/admin-main-running', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-running",{
    userInfo: req.session
  });
});
app.get('/admin-main-setup-time', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-setup-time",{
    userInfo: req.session
  });
});
app.get('/admin-main-load-time', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-load-time",{
    userInfo: req.session
  });
});
app.get('/admin-main-unload-time', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-unload-time",{
    userInfo: req.session
  });
});
app.get('/admin-main-pause-time', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-pause-time",{
    userInfo: req.session
  });
});
app.get('/admin-main-pending', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-pending",{
    userInfo: req.session
  });
});
app.get('/admin-main-replacetool', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-replacetool",{
    userInfo: req.session
  });
});
app.get('/admin-main-maintenance', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-maintenance",{
    userInfo: req.session
  });
});
app.get('/admin-main-outoforder', checkAuth, (req, res) => {
  req.session.home_url="/admin-menu"
  res.render("admin-ui/admin-main-outoforder",{
    userInfo: req.session
  });
});

app.get('/edit-popup', (req, res) => {
  res.render("admin-ui/edit-popup");
});
app.get('/edit-popupwithGPart', (req, res) => {
  res.render("admin-ui/edit-popupwithGPart");
});
app.get('/edit-popupwithreason', (req, res) => {
  res.render("admin-ui/edit-popupwithreason");
});
app.get('/edit-popupwithnote', (req, res) => {
  res.render("admin-ui/edit-popupwithnote");
});
app.get('/delete-popup', (req, res) => {
  res.render("admin-ui/delete-popup");
});
app.get('/admin-edit-role', (req, res) => {
  res.render("admin-ui/admin-edit-role");
});
app.get('/admin-edit-op-role', (req, res) => {
  res.render("admin-ui/admin-edit-op-role");
});

// End <-----


// Rest API Routes
app.use('/safran-iot/api/signup', signupRouter)
app.use('/safran-iot/api/login', loginRouter)
app.use('/safran-iot/api/outoforder', outOfOrderRouter)
app.use('/safran-iot/api/maintenance', maintenanceRouter)
app.use('/safran-iot/api/replacetool', replaceToolRouter)
app.use('/safran-iot/api/unfinishedtask', unfinishedTaskRouter)
app.use('/safran-iot/api/partmenu', partMenuRouter)
app.use('/safran-iot/api/pending', pendingRouter)
app.use('/safran-iot/api/admin', adminRouter)
app.use('/safran-iot/api/site', siteRouter)


app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})