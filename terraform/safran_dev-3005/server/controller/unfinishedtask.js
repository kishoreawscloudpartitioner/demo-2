const pool = require('../config/db')

const unfinishedTask = (req, res) => {
    const { machine_id } = req.body
    const operator_no = req.session.user_id
    const current_time = new Date()

    pool.query(
        `SELECT * FROM "SAFRAN"."INTERFACEACTIVITY" WHERE machine_id = $1 AND end_time IS NULL`,
        [machine_id],
        (err, results) => {
            if (err) {
                throw err
            }

            const rows_length = results.rows.length

            if (results.rows.length) {
                req.session.unfinishedtask_activity_id = results.rows[rows_length-1].activity_id
                const start_time = results.rows[rows_length-1].start_time

                const time_diffrence = (current_time, start_time) => {
                    let diffms = current_time - start_time
                    let diffDays = Math.floor(diffms / 86400000); // days
                    let diffHrs = Math.floor((diffms % 86400000) / 3600000) + (diffDays * 24); // hours
                    let diffMins = Math.round(((diffms % 86400000) % 3600000) / 60000); // minutes
                    let diffSec = Math.round((diffms / 1000) % 60); // seconds
                
                    return `${diffHrs}:${diffMins}:${diffSec}`
                }

                const resume_time = time_diffrence(current_time, start_time)

                console.log('resume time ', resume_time)
                console.log(current_time)

                res.status(201).json(
                    {
                        resume_time: resume_time,
                        activity_type_id: results.rows[rows_length-1].activity_type_id,
                        data: results.rows[rows_length-1]
                    }
                )
                
            } else {
                res.status(200).json({ message : 'No unfinished task'})
            }
            
        }
    )
}

const unfinishedTaskEnd = (req, res) => {
    const activity_id  = req.session.unfinishedtask_activity_id // TODO remove this from session
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."INTERFACEACTIVITY" SET end_time = $1 WHERE activity_id = $2 RETURNING *`,
        [end_time, activity_id],
        (err, results) => {
            if (err) {
                throw err
            }
            console.log(results.rows)
            res.status(201).json(
                {
                    message: `Unfinished task end time recorded for ${activity_id}`,
                }
            )
        }
    )
}


module.exports = {
    unfinishedTask,
    unfinishedTaskEnd
}