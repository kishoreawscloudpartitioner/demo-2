const pool = require('../config/db')
const crypto = require('crypto')


const signup = async (req, res) => {
    const { sign_id, first_name, last_name, email, phone_no, role_id, password } = req.body

    let errors = []

    if (!sign_id || !first_name || !last_name || !email || !phone_no || !role_id || !password) {
        errors.push({message: 'Please enter all fields'})
        res.status(401).json(errors)
    }

    else {
        const salt = 'safraniot'
    
        let passwordSalt = password + salt
        let passwordHash = await crypto.createHash('md5').update(passwordSalt).digest('hex')
    
        pool.query(
            `SELECT * FROM "SAFRAN"."USER" WHERE user_id = $1`,
            [sign_id],
            (err, results) =>{
                if (err){
                    console.log(err)
                    throw err
                }
                if (results.rows.length > 0) {
                    errors.push({message: `user with sign id : ${sign_id} already exist`})
                    res.status(401).json(errors)
                } else {
                    pool.query(
                        `CALL "SAFRAN"."ADDUSER"($1, $2, $3, $4, $5, $6, $7, $8)`,
                        [sign_id, first_name, last_name, email, phone_no, role_id, passwordHash, passwordSalt],
                        (err, results) => {
                            if (err){
                                throw err
                            }
                            res.status(201).json(
                                {
                                    message: 'user added',
                                    sign_id: sign_id,
                                    first_name: first_name,
                                    last_name: last_name,
                                    role_id: role_id
                                }
                            )
                        }
                    )
                }
            }
        )
    }
}

const login = async (req, res) => {
    const { sign_id, password } = req.body

    const salt = 'safraniot'
    let passwordSalt = password + salt

    let hashedEnterdPassword = await crypto.createHash('md5').update(passwordSalt).digest('hex')

    pool.query(
        `SELECT * FROM "SAFRAN"."USER" WHERE user_id = $1`,
        [sign_id],
        (err, results) => {
            if (err) {
                throw err
            }
            if (results.rows.length === 0){
                res.status(401).json({message: `User with ${sign_id} not found`})
            } else {
                const user = results.rows[0]
                
                
                req.session.user_id = user.user_id
                req.session.role_id = user.role_id
                req.session.first_name = user.first_name
                req.session.last_name = user.last_name
                req.session.is_logged_in = true
                console.log('Below is the SESSION Details')
                console.log(req.session)

                if (user.password_hash === hashedEnterdPassword){
                    res.status(200).json({
                        user_id :user.user_id,
                        first_name:user.first_name,
                        last_name: user.last_name,
                        role_id: user.role_id,
                        email: user.email,
                        phone_no: user.phone_no
                    })
                } else {
                    res.status(401).json({message : 'wrong password'})
                }
            }
        }
    )
}

module.exports = {
    signup,
    login,
}