const pool = require('../config/db')
const status = 'Insert'

const partmenu = (req, res) => {
    const { workorder_no, operation_no } = req.body
    
    pool.query(
        `SELECT "WORKORDERITEM"."part_no", "WORKORDERITEM"."part_name", "WORKORDERITEM"."ordered_qty_bu", "OPERATION"."description" FROM "SAFRAN"."WORKORDERITEM", "SAFRAN"."OPERATION" WHERE work_order_no = $1 AND operation_no = $2`,
        [workorder_no, operation_no],
        (err, results) => {
            if (err) {
                throw err
            }
            

            if (results.rows.length) {
                
                const data = results.rows[0]
                const part_no = data.part_no
                const part_desc = data.part_name
                const operation_desc = data.description
                const quantity = data.ordered_qty_bu

                req.session.partmenu_workorder_no = workorder_no
                req.session.partmenu_operation_no = operation_no
                req.session.partmenu_part_no = part_no
                req.session.partmenu_part_desc = part_desc
                req.session.partmenu_operation_desc = operation_desc
                req.session.partmenu_quantity = quantity

                res.status(200).json(
                    {
                        part_no,
                        part_desc,
                        operation_desc,
                        quantity
                    }
                )
            } else {
                res.status(500).json({ message: 'Internal server error (something went wrong)'})
            }
            
        }
    )
}

const startPart = (req, res) => {
    const { workorder_no, operation_no, site, machine_id, activity_type_id, quantity } = req.body
    req.session.partmenu_quantity = quantity
    const operator_no = req.session.user_id
    const part_no = req.session.partmenu_part_no
    const start_time = new Date()
    console.log(req.session)

    pool.query(
        `INSERT INTO "SAFRAN"."ACTIVITY" (work_order_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
        [workorder_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, start_time, status],
        (err, results) => {
            if (err) {
                throw err
            }

            req.session.partmenu_id = results.rows[0].id
            res.status(201).json(
                {
                    message: 'Data added succesfully for part menu in activity',
                    activity_type_id: activity_type_id,
                    data: results.rows
                }
            )
        }
    )
}

const load = (req, res) => {
    const { site, machine_id, activity_type_id } = req.body
    const id  = req.session.partmenu_id
    const workorder_no = req.session.partmenu_workorder_no
    const operation_no = req.session.partmenu_operation_no
    const operator_no = req.session.user_id
    const part_no = req.session.partmenu_part_no
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."ACTIVITY" SET end_time = $1 WHERE id = $2 RETURNING *`,
        [end_time, id],
        (err, results) => {
            if (err) {
                throw err
            }
            pool.query(
                `INSERT INTO "SAFRAN"."ACTIVITY" (work_order_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
                [workorder_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, end_time, status],
                (err, results) => {
                    if (err) {
                        throw err
                    }
                    req.session.partmenu_id = results.rows[0].id
                    res.status(201).json(
                        {
                            message: `Setup-complete end time recorded for ${id} and Load added successfully in activity on id ${results.rows[0].id}`,
                            data: results.rows[0]
                        }
                    )
                }
            )
        }
    )
}

const completeSetup = (req, res) => {
    const { site, machine_id } = req.body
    const id  = req.session.partmenu_id
    const workorder_no = req.session.partmenu_workorder_no
    const operation_no = req.session.partmenu_operation_no
    const operator_no = req.session.user_id
    const part_no = req.session.partmenu_part_no
    const quantity = req.session.partmenu_quantity
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."ACTIVITY" SET end_time = $1 WHERE id = $2 RETURNING *`,
        [end_time, id],
        (err, results) => {
            if (err) {
                throw err
            }
            pool.query(
                `INSERT INTO "SAFRAN"."PARTMENU" (work_order_no, site, machine_id, operator_no, part_no, operation_no, produce_part, start_time, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
                [workorder_no, site, machine_id, operator_no, part_no, operation_no, quantity, end_time, status],
                (err, results) => {
                    if (err) {
                        throw err
                    }
                    req.session.partmenu_partmenu_id = results.rows[0].id
                    res.status(201).json(
                        {
                            message: `Setup-complete or Load end time recorded for ${id} and Data added successfully in partmenu on id ${results.rows[0].id}`,
                            data: results.rows[0]
                        }
                    )
                }
            )
        }
    )
}

const finishPart = (req, res) => {
    const { site, machine_id, activity_type_id } = req.body
    const id = req.session.partmenu_partmenu_id
    const workorder_no = req.session.partmenu_workorder_no
    const operation_no = req.session.partmenu_operation_no
    const operator_no = req.session.user_id
    const part_no = req.session.partmenu_part_no
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."PARTMENU" SET end_time = $1 WHERE id = $2 RETURNING *`,
        [end_time, id],
        (err, results) => {
            if (err) {
                throw err
            }

            pool.query(
                `INSERT INTO "SAFRAN"."ACTIVITY" (work_order_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
                [workorder_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, end_time, status],
                (err, results) => {
                    if (err) {
                        throw err
                    }

                    req.session.partmenu_unload_id = results.rows[0].id
                    res.status(201).json(
                        {
                            message: `part end time recorded for id ${id} and Unload start time recorded in activity table`,
                            data: results.rows
                        }
                    )
                }
            )
        }
    )
}

const pause = (req, res) => {
    const { site, machine_id, activity_type_id, pause, comment } = req.body
    const workorder_no = req.session.partmenu_workorder_no
    const operation_no = req.session.partmenu_operation_no
    const operator_no = req.session.user_id
    const part_no = req.session.partmenu_part_no
    const start_time = new Date()

    if (pause === 'pause') {
        pool.query(
            `INSERT INTO "SAFRAN"."ACTIVITY" (work_order_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
            [workorder_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, start_time, status],
            (err, results) => {
                if (err) {
                    throw err
                }
    
                req.session.partmenu_pause_id = results.rows[0].id
                res.status(201).json(
                    {
                        message: `Data added successfully for pause`,
                        activity_type_id: activity_type_id,
                        data: results.rows[0]
                    }
                )
            }
        )

    } else{
        const pause_id = req.session.partmenu_pause_id
        pool.query(
            `UPDATE "SAFRAN"."ACTIVITY" SET end_time = $1, comment = $2 WHERE id = $3 RETURNING *`,
            [start_time, comment, pause_id],
            (err, results) => {
                if (err) {
                    throw err
                }
                res.status(201).json(
                    {
                        message: `End time recorded for pause(Resume) for id ${pause_id}`,
                        data: results.rows
                    }
                )
            }
        )

    }   
}

const returnOperation = (req, res) => {
    const unload_id = req.session.partmenu_unload_id
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."ACTIVITY" SET end_time = $1 WHERE id = $2 RETURNING *`,
        [end_time, unload_id],
        (err, results) => {
            if (err) {
                throw err
            }
            const activity_data = results.rows[0]
            res.status(201).json(
                {
                    message: `Unload end-time recorder for id ${unload_id}`,
                    activity_data: activity_data
                }
            )
        }
    )
}

const finish = (req, res) => {
    const { good_part } = req.body
    const id = req.session.partmenu_partmenu_id
    const unload_id = req.session.partmenu_unload_id
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."PARTMENU" SET good_part = $1 WHERE id = $2 RETURNING *`,
        [good_part, id],
        (err, results) => {
            if (err) {
                throw err
            }
            const partmenu_data = results.rows[0]

            pool.query(
                `UPDATE "SAFRAN"."ACTIVITY" SET end_time = $1 WHERE id = $2 RETURNING *`,
                [end_time, unload_id],
                (err, results) => {
                    if (err) {
                        throw err
                    }
                    const activity_data = results.rows[0]
                    res.status(201).json(
                        {
                            message: `Good Part added successfully on id ${id} and Unload end-time recorder for id ${unload_id}`,
                            partmenu_row: partmenu_data,
                            activity_data: activity_data
                        }
                    )
                }
            )
        }
    )
}

const quality = (req, res) => {

    const _multiInsert = arrOfValues => {
        // removes lastCharacter
        const _remLastChar = str => str.slice(0, str.length - 1);
    
          let foramttedQuery = '';
    
          arrOfValues.forEach(row => {
            let newRow = '';
            for (const val of Object.values(row)) {
              let newValue = '';
              if (typeof val === 'string') newValue = `'${val}',`;
              else newValue = `${val},`;
              newRow = newRow.concat(newValue);
            }
    
            foramttedQuery = foramttedQuery.concat(`(${_remLastChar(newRow)}),`);
          });
    
        return _remLastChar(foramttedQuery);
    };

    const { site, machine_id, good_part, quality_reason_ids } = req.body
    let qualityreason_ids = quality_reason_ids.split(',')
    const id = req.session.partmenu_partmenu_id
    const workorder_no = req.session.partmenu_workorder_no
    const operator_no = req.session.user_id
    const part_no = req.session.partmenu_part_no
    const operation_no = req.session.partmenu_operation_no
    const unload_id = req.session.partmenu_unload_id 
    const end_time = new Date()

    let values_arr = []

    qualityreason_ids
        .forEach((quality_reason_id) => {
            values_arr.push(
                {
                    workorder_no,
                    site,
                    machine_id,
                    operator_no,
                    part_no,
                    operation_no,
                    quality_reason_id ,
                    status 
                }
            )
        })
    
    const query_template = `INSERT INTO "SAFRAN"."QUALITY" (work_order_no, site, machine_id, operator_no, part_no, operation_no, quality_reason_id, status) VALUES ${_multiInsert(values_arr)} RETURNING *`

    pool.query(
        `UPDATE "SAFRAN"."PARTMENU" SET good_part = $1 WHERE id = $2 RETURNING *`,
        [good_part, id],
        (err, results) => {
            if (err) {
                console.log('error from partmenu GOOD PART')
                console.log(err)
                throw err
            }

            const partmenu_data = results.rows[0]

            pool.query(
                `UPDATE "SAFRAN"."ACTIVITY" SET end_time = $1 WHERE id = $2 RETURNING *`,
                [end_time, unload_id],
                (err, results) => {
                    if (err) {
                        console.log('error from partmenu UNLOAD END TIME')
                        console.log(err)
                        throw err
                    }

                    const activity_data = results.rows[0]

                    pool.query(
                        query_template,
                        (err, results) => {
                            if (err) {
                                console.log('error from partmenu QUALITY REASONS')
                                console.log(err)
                                throw err
                            }

                            const quality_data = results.rows
                
                            res.status(201).json(
                                {
                                    message: `Good Part added successfully on id ${id} and Unload end-time recorded for id ${unload_id} and Quality reasons recorded in quality table`,
                                    partmenu_data: partmenu_data,
                                    activity_data: activity_data,
                                    quality_data: quality_data
                                }
                            )
                        }
                    )
                }
            )
        }
    )
}

module.exports = {
    partmenu,
    startPart,
    completeSetup,
    load,
    finishPart,
    pause,
    returnOperation,
    finish,
    quality
}