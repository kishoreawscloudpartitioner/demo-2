const pool = require('../config/db')
const status = 'Insert'

const maintenanceMaintenanceDropdown = (req, res) => {
    pool.query(
        `SELECT * FROM "SAFRANOEE"."INTERFACEACTIVITYREASON" WHERE activity_type = 1004 AND reason_for = 'Maintenance' ORDER BY reason_id ASC`,
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(200).json(results.rows)
        }
    )
}

const maintenanceOperatorDropdown = (req, res) => {
    pool.query(
        `SELECT * FROM "SAFRANOEE"."INTERFACEACTIVITYREASON" WHERE activity_type = 1004 AND reason_for = 'Operator' ORDER BY reason_id ASC`,
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(200).json(results.rows)
        }
    )
}

const maintenanceStart = (req, res) => {
    const { site, machine_id, activity_type_id } = req.body
    const operator_no = req.session.user_id
    const start_time = new Date()

    pool.query(
        `INSERT INTO "SAFRAN"."INTERFACEACTIVITY" (site, machine_id, operator_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
        [site, machine_id, operator_no, activity_type_id, start_time, status],
        (err, results) => {
            if (err) {
                throw err
            }
            req.session.maintenance_activity_id = results.rows[0].activity_id
            res.status(201).json(
                {
                    message: 'Data added succesfully for maintenance',
                    activity_id: results.rows[0].activity_id
                }
            )
        }
    )
}

const maintenanceEnd = (req, res) => {
    const { reason_id } = req.body
    const activity_id  = req.session.maintenance_activity_id
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."INTERFACEACTIVITY" SET reason_id = $1, end_time = $2 WHERE activity_id = $3 RETURNING *`,
        [reason_id, end_time, activity_id],
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(201).json(
                {
                    message: `Maintenance end time recorded for ${activity_id}`,
                }
            )
        }
    )
}

module.exports = {
    maintenanceStart,
    maintenanceEnd
}