const pool = require('../../../config/db')

async function check (sql_query) {
    try {
        const data = await pool.query(sql_query)
        return data
    } catch (err) {
        return err
    }
}

async function list(activity_type_id, start_date, end_date, machine_id = 0, operator_no = 0, workorder_no = 0, site = 0) {
    let sql_query = `SELECT * FROM "SAFRAN"."ACTIVITY" WHERE activity_type_id = ${activity_type_id} AND 
    start_time BETWEEN '${start_date}' AND '${end_date}'::date + INTERVAL '1 day' `
    let error_list = []

    if (machine_id) {
        let query = `SELECT * FROM "SAFRAN"."MACHINE" WHERE machine_id = '${machine_id}'`
        const machine_data = await check(query)
        if (machine_data.rows.length) {
            sql_query = sql_query + `AND machine_id = '${machine_id}' `
        } else{
            error_list.push({message: `Machine with ${machine_id} id does not exist`})
        }
    }

    if (operator_no) {
        let query = `SELECT * FROM "SAFRAN"."USER" WHERE user_id = '${operator_no}'`
        const operator_data = await check(query)
        if (operator_data.rows.length) {
            sql_query = sql_query + `AND operator_no = '${operator_no}' `
        } else{
            error_list.push({message: `Operator with ${operator_no} number does not exist`})
        }  
    }

    if (workorder_no) {
        let query = `SELECT * FROM "SAFRAN"."WORKORDERITEM" WHERE work_order_no = '${workorder_no}'`
        const workorder_data = await check(query)
        if (workorder_data.rows.length) {
            sql_query = sql_query + `AND work_order_no = '${workorder_no}' `
        } else{
            error_list.push({message: `Work order with ${workorder_no} number does not exist`})
        }  
    }
    if (site) {
        sql_query += `AND site = '${site}' `
    }

    sql_query += `AND status != 'Delete' ORDER BY id`
    
    if (error_list.length) {
        return error_list
    } else {
        const list_data = await pool.query(sql_query)
        return list_data
    }
}

async function single(id, activity_type_id) {
    const single = await pool.query(`SELECT * FROM "SAFRAN"."ACTIVITY" WHERE id = $1 AND activity_type_id = $2`, [id, activity_type_id])
    return single
}

module.exports = {
    check,
    list,
    single
}