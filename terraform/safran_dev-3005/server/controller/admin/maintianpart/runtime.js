const pool = require('../../../config/db')
const check = require('./search').check
const helper = require('../../../utils/helper')

const getData = async (req, res) => {
    const { start_date, end_date, machine_id, operator_no, workorder_no, site } = req.body

    let sql_query = `SELECT * FROM "SAFRAN"."PARTMENU" WHERE start_time BETWEEN '${start_date}' AND '${end_date}'::date + INTERVAL '1 day' `
    let error_list = []

    if (machine_id) {
        let query = `SELECT * FROM "SAFRAN"."MACHINE" WHERE machine_id = '${machine_id}'`
        const machine_data = await check(query)
        if (machine_data.rows.length) {
            sql_query = sql_query + `AND machine_id = '${machine_id}' `
        } else{
            error_list.push({message: `Machine with ${machine_id} id does not exist`})
        }
    }
    if (operator_no) {
        let query = `SELECT * FROM "SAFRAN"."USER" WHERE user_id = '${operator_no}'`
        const operator_data = await check(query)
        if (operator_data.rows.length) {
            sql_query = sql_query + `AND operator_no = '${operator_no}' `
        } else{
            error_list.push({message: `Operator with ${operator_no} number does not exist`})
        }  
    }
    if (workorder_no) {
        let query = `SELECT * FROM "SAFRAN"."WORKORDERITEM" WHERE work_order_no = '${workorder_no}'`
        const workorder_data = await check(query)
        if (workorder_data.rows.length) {
            sql_query = sql_query + `AND work_order_no = '${workorder_no}' `
        } else{
            error_list.push({message: `Work order with ${workorder_no} number does not exist`})
        }  
    }
    if (site) {
        sql_query += `AND site = '${site}' `
    }

    sql_query = sql_query + `AND status != 'Delete' ORDER BY id`
    
    if (error_list.length) {
        res.status(404).json(error_list)
    } else {
        pool.query(
            sql_query,
            (err, results) => {
                if (err) {
                    throw err
                }
                res.status(200).json({
                    data: results.rows
                })
            }
        )
    }
}

const getSingleData = (req, res) => {
    try {
        const { id } = req.body
        pool.query(
            `SELECT * FROM "SAFRAN"."PARTMENU" WHERE id = $1`,
            [id],
            (err, results) => {
                if (err) {
                    throw err
                }

                if (results.rows.length) {
                    try {
                        const single_data = results.rows[0]

                        const good_part = single_data.good_part
                        const got_start_time = single_data.start_time
                        const got_end_time = single_data.end_time

                        const start_date = helper.get_date(got_start_time)
                        const start_time = helper.get_time(got_start_time)
                        const end_date = helper.get_date(got_end_time)
                        const end_time = helper.get_time(got_end_time)

                        res.status(200).json(
                            {
                                good_part,
                                start_date,
                                start_time,
                                end_date,
                                end_time
                            }
                        )

                    } catch (error) {
                        res.status(500).json({message: error.message})
                    }

                } else {
                    res.status(200).json({ message: `Data with ${id} does not exist` })
                }
            }
        )
    } catch (error) {
        res.status(500).json({message: error.message})
    }
}

const update = (req, res) => {
    try {
        const { id, good_part, start_time, end_time } = req.body
        const change_by = req.session.user_id
    
        pool.query(
            `CALL "SAFRAN"."UPDATE_RUNTIME"($1, $2, $3, $4, $5)`,
            [id, good_part, start_time, end_time, change_by],
            (err, results) => {
                if (err) {
                    throw err
                }
    
                res.status(201).json({ message: `Runtime Update complete for id ${id}`})
            }
        )
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

const delete_part = (req, res) => {
    const { id } = req.body 
    const change_by = req.session.user_id

    pool.query(
        `CALL "SAFRAN"."DELETE_RUNTIME"($1, $2)`,
        [id, change_by],
        (err, results) => {
            if (err) {
                throw err
            }
            
            res.status(201).json({ message: `Delete runtime with id ${id}`})
        }
    )
}

module.exports = {
    getData,
    getSingleData,
    update,
    delete_part,
}