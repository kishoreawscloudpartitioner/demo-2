const search = require('./search')
const helper = require('../../../utils/helper')
const change = require('./change')

const getData = async (req, res) => {
    const activity_type_id = 100001
    const { start_date, end_date, machine_id, operator_no, workorder_no, site} = req.body
    const list_data = await search.list(activity_type_id, start_date, end_date, machine_id, operator_no, workorder_no, site)
    
    if (!list_data.rows) {
        res.status(201).json(list_data)
    } else {
        res.status(200).json({data:list_data.rows})
    }
}

const getSingleData = async (req, res) => {
    try {
        const { id } = req.body
        const activity_type_id = 100001
        const single_data = await search.single(id, activity_type_id)

        const got_start_time = single_data.rows[0].start_time
        const got_end_time = single_data.rows[0].end_time

        const start_date = helper.get_date(got_start_time)
        const start_time = helper.get_time(got_start_time)
        const end_date = helper.get_date(got_end_time)
        const end_time = helper.get_time(got_end_time)

        res.status(200).json(
            {
                start_date,
                start_time,
                end_date,
                end_time
            }
        )
    } catch (error) {
        res.status(500).json({message: error.message})
    }
}

const update = async (req, res) => {
    try {
        const activity_type_id = 100001
        const { id, start_time, end_time } = req.body
        const change_by = req.session.user_id
        console.log('Updating')
        const update_record = await change.update(id, activity_type_id, start_time, end_time, change_by)
        console.log(update_record)
        res.status(201).json({ message: `Setup Time Update complete for id ${id}`})

    } catch (error) {
        console.log(error)
        res.status(500).json({message: error.message})
    }
}

const delete_setup = async (req, res) => {
    try {
        const activity_type_id = 100001
        const { id } = req.body
        const change_by = req.session.user_id
        console.log('updating')
        const delete_record = await change.delete_row(id, activity_type_id, change_by)
        res.status(201).json({ message: `Setup Time deleted with id ${id}`})

    } catch (error) {
        console.log(error)
        res.status(500).json({message: error.message})
    }
}

module.exports = {
    getData,
    getSingleData,
    update,
    delete_setup
}