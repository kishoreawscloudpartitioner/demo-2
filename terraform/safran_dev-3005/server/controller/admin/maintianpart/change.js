const pool = require('../../../config/db')

async function update(id, activity_type_id, start_time, end_time, modify_by) {
    const update = await pool.query(`CALL "SAFRAN"."UPDATE_ACTIVITY"($1, $2, $3, $4, $5)`, 
    [id, activity_type_id, start_time, end_time, modify_by])
    return update
}

async function delete_row(id, activity_type_id, modify_by) {
    const deleted = await pool.query(`CALL "SAFRAN"."DELETE_ACTIVITY"($1, $2, $3)`, 
    [id, activity_type_id, modify_by])
    return deleted
}

module.exports = {
    update,
    delete_row
}