const pool = require('../../config/db')
const helper = require('../../utils/helper')

const getData = async (req, res) => {
    const { start_date, end_date, machine_id, operator_no, site } = req.body
    const activity_type_id = 1004

    let sql_query = `SELECT * FROM "SAFRAN"."INTERFACEACTIVITY_VIEW" WHERE activity_type_id = ${activity_type_id}
    AND start_time BETWEEN '${start_date}' AND '${end_date}'::date + INTERVAL '1 day' `
    let error_list = []

    if (machine_id) {
        let query = `SELECT * FROM "SAFRAN"."MACHINE" WHERE machine_id = '${machine_id}'`
        const machine_data = await helper.query_runner(query)
        if (machine_data.rows.length) {
            sql_query += `AND machine_id = '${machine_id}' `
        } else{
            error_list.push({message: `Machine with ${machine_id} id does not exist`})
        }
    }
    if (operator_no) {
        let query = `SELECT * FROM "SAFRAN"."USER" WHERE user_id = '${operator_no}'`
        const operator_data = await helper.query_runner(query)
        if (operator_data.rows.length) {
            sql_query += `AND operator_no = '${operator_no}' `
        } else{
            error_list.push({message: `Operator with ${operator_no} number does not exist`})
        }  
    }
    if (site) {
        sql_query += `AND site = '${site}' `
    }
    
    sql_query += `AND status != 'Delete' ORDER BY activity_id`

    if (error_list.length) {
        res.status(404).json(error_list)
    } else {
        pool.query(
            sql_query,
            (err, results) => {
                if (err) {
                    throw err
                }
                res.status(200).json({
                    data: results.rows
                })
            }
        )
    }
}

const singleData = async (req, res) => {
    try {
        const { id, activity_type_id } = req.body
    
        const single_data = await pool.query(
            `SELECT * FROM "SAFRAN"."INTERFACEACTIVITY" WHERE activity_id = $1 AND activity_type_id = $2`,
            [id, activity_type_id]
        )
        const maintenance_data = await single_data.rows[0]
        const operator_no = maintenance_data.operator_no
        const got_start_date = maintenance_data.start_time
        const got_end_date = maintenance_data.end_time
        const reason_id = maintenance_data.reason_id

        const start_date = helper.get_date(got_start_date)
        const start_time = helper.get_time(got_start_date)
        const end_date = helper.get_date(got_end_date)
        const end_time = helper.get_time(got_end_date)
    
        const resaons_data = await pool.query(
            `SELECT * from "SAFRAN"."List_of_reason"($1, $2)`, // TODO: New view or function not getting reason from ui
            [operator_no, activity_type_id]
        )
        const reasons_list = await resaons_data.rows
    
        res.status(200).json({
            operator_no,
            start_date,
            start_time,
            end_date,
            end_time,
            reason_id,
            reasons_list
        })
        
    } catch (error) {
        res.status(500).json({
            message: error.message
        })   
    }
}

const update = (req, res) => {
    try {
        const { id, activity_type_id, reason_id, start_time, end_time } = req.body
        const change_by = req.session.user_id

        pool.query(
            `CALL "SAFRAN"."UPDATE_OUT_OF_ORDER_MAINTENANCE"($1, $2, $3, $4, $5, $6)`,
            [id, activity_type_id, reason_id, start_time, end_time, change_by],
            (err, results) => {
                if (err) {
                    throw err
                }
    
                res.status(201).json({ message: `Maintenance Update complete for id ${id}`})
            }
        )
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

const deleteMaintenance = (req, res) => {
    try {
        const { id } = req.body
        const change_by = req.session.user_id
        pool.query(
            `CALL "SAFRAN"."DELETE_INTERFACEACTIVITY"($1, $2)`,
            [id, change_by],
            (err, results) => {
                if (err) {
                    throw err
                }
    
                res.status(201).json({ message: `Maintenance Delete complete for id ${id}`})
            }
        )
    } catch (error) {
        res.staus(500).json({
            message: error.message
        })
    }

}


module.exports = {
    getData,
    singleData,
    update,
    deleteMaintenance
}