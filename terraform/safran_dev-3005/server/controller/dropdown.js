const pool = require('../config/db')

const dropdown = (req, res) => {
    const { machine_type, activity_type_id } = req.body
    const reason_for = req.session.role_id
    
    pool.query(
        `SELECT * FROM "SAFRAN"."INTERFACEACTIVITYREASON" WHERE activity_type = $1 AND reason_for = $2 AND machine_type = $3 ORDER BY reason_id ASC`,
        [activity_type_id, reason_for, machine_type],
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(200).json(results.rows)
        }
    )
}

const qualityDropdown = (req, res) => {
    pool.query(
        `SELECT * FROM "SAFRAN"."QUALITYREASON" ORDER BY quality_reason_id ASC`,
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(200).json(results.rows)
        }
    )
}

const siteDropdown = async (req, res) => {
    try {
        const sites = await pool.query(`SELECT * FROM "SAFRAN"."SITE"ORDER BY site_id ASC`)
        res.status(200).json(sites.rows)
    } catch (error) {
        res.status(500).json(error.message)
    }   
}

module.exports = {
    dropdown,
    qualityDropdown,
    siteDropdown
}