const pool = require('../config/db')
const status = 'Insert'

const replaceToolStart = (req, res) => {
    const { site, machine_id } = req.body
    const operator_no = req.session.user_id
    const activity_type_id = 1002
    const start_time = new Date()

    pool.query(
        `INSERT INTO "SAFRAN"."INTERFACEACTIVITY" (site, machine_id, operator_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
        [site, machine_id, operator_no, activity_type_id, start_time, status],
        (err, results) => {
            if (err) {
                throw err
            }
            req.session.replacetool_activity_id = results.rows[0].activity_id
            res.status(201).json(
                {
                    message: 'Data added succesfully for replace tool',
                    activity_id: results.rows[0].activity_id
                }
            )
        }
    )
}

const replaceToolEnd = (req, res) => {
    const { description } = req.body
    const activity_id  = req.session.replacetool_activity_id
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."INTERFACEACTIVITY" SET description = $1, end_time = $2 WHERE activity_id = $3 
        RETURNING *`,
        [description, end_time, activity_id],
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(201).json(
                {
                    message: `Replace tool end time recorded for ${activity_id}`,
                }
            )
        }
    )
}

module.exports = {
    replaceToolStart,
    replaceToolEnd
}