const pool = require('../config/db')

const status = 'Insert'

const pendingStart = (req, res) => {
    const { site, machine_id, activity_type_id } = req.body
    const operator_no = req.session.user_id
    const start_time = new Date()

    pool.query(
        `INSERT INTO "SAFRAN"."INTERFACEACTIVITY" (site, machine_id, operator_no, activity_type_id, start_time, status) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`,
        [site, machine_id, operator_no, activity_type_id, start_time, status],
        (err, results) => {
            if (err) {
                throw err
            }
            req.session.pending_activity_id = results.rows[0].activity_id
            res.status(201).json(
                {
                    message: 'Data Added succesfully for pending',
                    activity_id: results.rows[0].activity_id
                }
            )
        }
    )
}

const pendingEnd = (req, res) => {
    const { reason_id } = req.body
    const activity_id  = req.session.pending_activity_id
    const end_time = new Date()

    pool.query(
        `UPDATE "SAFRAN"."INTERFACEACTIVITY" SET reason_id = $1, end_time = $2 WHERE activity_id = $3 RETURNING *`,
        [reason_id, end_time, activity_id],
        (err, results) => {
            if (err) {
                throw err
            }
            res.status(201).json(
                {
                    message: `Pending end time recorded for ${activity_id}`,
                }
            )
        }
    )
}

module.exports = {
    pendingStart,
    pendingEnd
}


// POSTGRESQL QUERY:
// INSERT INTO "SAFRANOEE"."PENDING" (work_order_no, site, machine_id, operator_no, part_no, operation_no, activity_type_id, reason_id)
// VALUES ('workorder', 'site', 'machine', 'operator', 'part', 'operation', 1003, 10013)

// TO BE QUERY:
// SELECT "INTERFACEACTIVITYREASON"."reason_id", "INTERFACEACTIVITYREASON"."reason" FROM  "SAFRAN"."INTERFACEACTIVITYREASON" WHERE activity_type = 1003 AND "SAFRAN"."INTERFACEACTIVITYREASON"."machine_type" =(SELECT machine_type FROM "SAFRAN"."MACHINE" WHERE machine_id = $1)