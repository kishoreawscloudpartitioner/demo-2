resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"  # Update with your desired VPC CIDR block
}

resource "aws_subnet" "my_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"  # Update with your desired subnet CIDR block
}

resource "aws_security_group" "webserver_sg" {
  name        = "webserver-sg"
  description = "Security group for the webserver instance"
  vpc_id      = aws_vpc.my_vpc.id
  
  // Define inbound and outbound rules as needed
  // Example rules:
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "webserver" {
  ami           = "ami-0a1179631ec8933d7"  # Update with your desired AMI ID
  instance_type = "t2.micro"               # Update with your desired instance type
  key_name      = "safran_keypair"         # Update with your key pair name
  subnet_id     = aws_subnet.my_subnet.id  # Specify the subnet ID within your VPC
  security_groups = [aws_security_group.webserver_sg.name]

  tags = {
    Name = "webserver"
  }

  provisioner "file" {
    source      = "./safran_dev-3005"
    destination = "/home/ec2-user/safran_dev-3005"
  }

  provisioner "remote-exec" {
    inline = [
      "curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash",
      ". ~/.nvm/nvm.sh",
      "nvm install 16",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "cd safran_dev-3005",
      "mv ns.txt .env",
      ". ~/.nvm/nvm.sh",
      "npm i",
      "npm i -g pm2",
      "cd server",
      "pm2 start server.js",
    ]
  }
}
